<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function locarization($lang)
    {

        session()->put('lang',$lang);
        return redirect()->back();
    }
}
