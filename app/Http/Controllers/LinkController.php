<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LinkController extends Controller
{


    public function index()
    {

    	return view('home.index');
    }

    public function index2()
    {
    	return view('search.list');
    }

    public function index3()
    {
    	return view('search.grid');
    }

    public function index4()
    {
    	return view('doctors.details');
    }

     public function index5()
    {
    	return view('doctors.articles');
    }
    public function index5_1()
    {
        return view('doctors.article_detials');
    }

     public function index6()
    {
    	return view('doctors.faq');
    }
     public function index6_1()
    {
        return view('doctors.faq_slug');
    }

     public function index7()
    {
    	return view('doctors.ticket');
    }
    public function index8()
    {
        return view('doctors.question');
    }



    // dashboard doctord

     public function index9()
    {
        return view('doctor_dashboard.dashboard');
    }
    public function index10()
    {
        return view('doctor_dashboard.setting');
    }
    public function index11()
    {
        return view('doctor_dashboard.articles');
    }
    public function index12()
    {
        return view('doctor_dashboard.new_articles');
    }

    public function index13()
    {
        return view('doctor_dashboard.faq');
    }
    public function index14()
    {
        return view('doctor_dashboard.new_faq');
    }
    public function index15()
    {
        return view('doctor_dashboard.ticket');
    }
    public function index16()
    {
        return view('doctor_dashboard.ticket_reserved');
    }

    public function index17()
    {
        return view('doctor_dashboard.question');
    }
     public function index18()
    {
        return view('doctor_dashboard.question_answered');
    }



    // .\ dashboard doctord

    //  dashboard users 

    public function index19()
    {
        return view('user.dashboard');
    }

    public function index20()
    {
        return view('user.records');
    }
    public function index21()
    {
        return view('user.favorites');
    }
    public function index22()
    {
        return view('user.ticket');
    }
    public function index23()
    {
        return view('user.liders');
    }
     public function index24()
    {
        return view('user.Mylider');
    }

     public function index25()
    {
        return view('user.questions');
    }
     public function index26()
    {
        return view('user.question_answered');
    }
     public function index27()
    {
        return view('user.setting');
    }



    // .\ dashboard users


    //  dashboard lider


     public function index28()
    {
        return view('lider.dashboard');
    }
     public function index29()
    {
        return view('lider.setting');
    }
     public function index30()
    {
        return view('lider.patients');
    }

    // .\ dashboard lider

    //  articles lider

    public function index31()
    {
        return view('articles.index');
    }

    // .\ articles lider




}
