
var initMap = function() {
  var s = document.createElement("script");
  s.type = "text/javascript";
  s.src = "infobox.js";
  $("map_listing").append(s);
  var map = new google.maps.Map(document.getElementById('map_listing'), {
    center: {lat: 33.78444500, lng: -118.15378000},
    zoom: 8
  });
}


var mapObject,
markers=[],
markersData = {
        "\x44\x6F\x63\x74\x6F\x72\x73": [{
                name: "Dr. Jhoanna Steel",
                location_latitude: 48.873792,
                location_longitude: 2.295028,
                map_image_url: "img/doctor_listing_1.jpg",
                type: "Psicologist - Pediatrician",
                url_detail: "detail-page.html",
                name_point: "Dr. Jhoanna Steel",
                description_point: "35 Newtownards Road, Belfast, BT4.",
                get_directions_start_address: "",
                phone: "+3934245255"
            },
            {
                name: "Dr. Robert Carl",
                location_latitude: 48.800040,
                location_longitude: 2.139670,
                map_image_url: "img/doctor_listing_1.jpg",
                type: "Psicologist",
                url_detail: "detail-page.html",
                name_point: "Dr. Robert Carl",
                description_point: "35 Newtownards Road, Belfast, BT4.",
                get_directions_start_address: "",
                phone: "+3934245255"
            },
            {
                name: "Dr. Mark Twain",
                location_latitude: 48.846222,
                location_longitude: 2.346414,
                map_image_url: "img/doctor_listing_1.jpg",
                type: "Primary Care",
                url_detail: "detail-page.html",
                name_point: "Dr. Mark Twain",
                description_point: "35 Newtownards Road, Belfast, BT4.",
                get_directions_start_address: "",
                phone: "+3934245255"
            }
        ]
    }


;
var mapOptions= {
    zoom:10,
    center: new google[_0xbaa2[15]].LatLng(48.865633, 2.321236),
    mapTypeId:google[_0xbaa2[15]][_0xbaa2[17]][_0xbaa2[16]],
    mapTypeControl:false,
    mapTypeControlOptions: {
        style: google[_0xbaa2[15]][_0xbaa2[19]][_0xbaa2[18]], position: google[_0xbaa2[15]][_0xbaa2[21]][_0xbaa2[20]]
    }
    ,
    panControl:false,
    panControlOptions: {
        position: google[_0xbaa2[15]][_0xbaa2[21]][_0xbaa2[22]]
    }
    ,
    zoomControl:true,
    zoomControlOptions: {
        style: google[_0xbaa2[15]][_0xbaa2[24]][_0xbaa2[23]], position: google[_0xbaa2[15]][_0xbaa2[21]][_0xbaa2[25]]
    }
    ,
    scrollwheel:false,
    scaleControl:false,
    scaleControlOptions: {
        position: google[_0xbaa2[15]][_0xbaa2[21]][_0xbaa2[20]]
    }
    ,
    streetViewControl:true,
    streetViewControlOptions: {
        position: google[_0xbaa2[15]][_0xbaa2[21]][_0xbaa2[25]]
    }
    ,
    styles:[ {
        "\x66\x65\x61\x74\x75\x72\x65\x54\x79\x70\x65":_0xbaa2[26],
        "\x73\x74\x79\x6C\x65\x72\x73":[ {
            "\x68\x75\x65": _0xbaa2[27]
        }
        ,
        {
            "\x73\x61\x74\x75\x72\x61\x74\x69\x6F\x6E": 43.400000000000006
        }
        ,
        {
            "\x6C\x69\x67\x68\x74\x6E\x65\x73\x73": 37.599999999999994
        }
        ,
        {
            "\x67\x61\x6D\x6D\x61": 1
        }
        ]
    }
    ,
    {
        "\x66\x65\x61\x74\x75\x72\x65\x54\x79\x70\x65":_0xbaa2[28],
        "\x73\x74\x79\x6C\x65\x72\x73":[ {
            "\x68\x75\x65": _0xbaa2[29]
        }
        ,
        {
            "\x73\x61\x74\x75\x72\x61\x74\x69\x6F\x6E": -61.8
        }
        ,
        {
            "\x6C\x69\x67\x68\x74\x6E\x65\x73\x73": 45.599999999999994
        }
        ,
        {
            "\x67\x61\x6D\x6D\x61": 1
        }
        ]
    }
    ,
    {
        "\x66\x65\x61\x74\x75\x72\x65\x54\x79\x70\x65":_0xbaa2[30],
        "\x73\x74\x79\x6C\x65\x72\x73":[ {
            "\x68\x75\x65": _0xbaa2[31]
        }
        ,
        {
            "\x73\x61\x74\x75\x72\x61\x74\x69\x6F\x6E": -100
        }
        ,
        {
            "\x6C\x69\x67\x68\x74\x6E\x65\x73\x73": 51.19999999999999
        }
        ,
        {
            "\x67\x61\x6D\x6D\x61": 1
        }
        ]
    }
    ,
    {
        "\x66\x65\x61\x74\x75\x72\x65\x54\x79\x70\x65":_0xbaa2[32],
        "\x73\x74\x79\x6C\x65\x72\x73":[ {
            "\x68\x75\x65": _0xbaa2[31]
        }
        ,
        {
            "\x73\x61\x74\x75\x72\x61\x74\x69\x6F\x6E": -100
        }
        ,
        {
            "\x6C\x69\x67\x68\x74\x6E\x65\x73\x73": 52
        }
        ,
        {
            "\x67\x61\x6D\x6D\x61": 1
        }
        ]
    }
    ,
    {
        "\x66\x65\x61\x74\x75\x72\x65\x54\x79\x70\x65":_0xbaa2[33],
        "\x73\x74\x79\x6C\x65\x72\x73":[ {
            "\x68\x75\x65": _0xbaa2[34]
        }
        ,
        {
            "\x73\x61\x74\x75\x72\x61\x74\x69\x6F\x6E": -13.200000000000003
        }
        ,
        {
            "\x6C\x69\x67\x68\x74\x6E\x65\x73\x73": 2.4000000000000057
        }
        ,
        {
            "\x67\x61\x6D\x6D\x61": 1
        }
        ]
    }
    ,
    {
        "\x66\x65\x61\x74\x75\x72\x65\x54\x79\x70\x65":_0xbaa2[35],
        "\x73\x74\x79\x6C\x65\x72\x73":[ {
            "\x68\x75\x65": _0xbaa2[36]
        }
        ,
        {
            "\x73\x61\x74\x75\x72\x61\x74\x69\x6F\x6E": -1.0989010989011234
        }
        ,
        {
            "\x6C\x69\x67\x68\x74\x6E\x65\x73\x73": 11.200000000000017
        }
        ,
        {
            "\x67\x61\x6D\x6D\x61": 1
        }
        ]
    }
    ]
}

;
var marker;
mapObject=new google[_0xbaa2[15]].Map(document[_0xbaa2[38]](_0xbaa2[37]), mapOptions);
for(var key in markersData) {
    markersData[key][_0xbaa2[1]](function(_0x69fexc) {
        marker=new google[_0xbaa2[15]].Marker( {
            position: new google[_0xbaa2[15]].LatLng(_0x69fexc[_0xbaa2[39]], _0x69fexc[_0xbaa2[40]]), map: mapObject, icon: _0xbaa2[41]+ key+ _0xbaa2[42]
        }
        );
        if(_0xbaa2[43]===typeof markers[key]) {
            markers[key]=[]
        }
        ;
        markers[key][_0xbaa2[44]](marker);
        google[_0xbaa2[15]][_0xbaa2[49]][_0xbaa2[48]](marker, _0xbaa2[45], (function() {
            closeInfoBox();
            getInfoBox(_0x69fexc)[_0xbaa2[46]](mapObject, this);
            mapObject[_0xbaa2[47]]( new google[_0xbaa2[15]].LatLng(_0x69fexc[_0xbaa2[39]], _0x69fexc[_0xbaa2[40]]))
        }
        ))
    }
    )
}

;
function hideAllMarkers() {
    for(var key in markers) {
        markers[key][_0xbaa2[1]](function(marker) {
            marker[_0xbaa2[50]](null)
        }
        )
    }
}

function toggleMarkers(_0x69fexf) {
    hideAllMarkers();
    closeInfoBox();
    if(_0xbaa2[43]===typeof markers[_0x69fexf]) {
        return false
    }
    ;
    markers[_0x69fexf][_0xbaa2[1]](function(marker) {
        marker[_0xbaa2[50]](mapObject);
        marker[_0xbaa2[52]](google[_0xbaa2[15]][_0xbaa2[51]].DROP)
    }
    )
}

function closeInfoBox() {
    $(_0xbaa2[54])[_0xbaa2[53]]()
}

function getInfoBox(_0x69fexc) {
    return new InfoBox( {
        content: _0xbaa2[55]+ _0xbaa2[56]+ _0x69fexc[_0xbaa2[57]]+ _0xbaa2[58]+ _0x69fexc[_0xbaa2[59]]+ _0xbaa2[60]+ _0xbaa2[61]+ _0x69fexc[_0xbaa2[62]]+ _0xbaa2[63]+ _0xbaa2[64]+ _0x69fexc[_0xbaa2[57]]+ _0xbaa2[65]+ _0x69fexc[_0xbaa2[66]]+ _0xbaa2[67]+ _0xbaa2[68]+ _0x69fexc[_0xbaa2[69]]+ _0xbaa2[70]+ _0xbaa2[71]+ _0xbaa2[72]+ _0x69fexc[_0xbaa2[73]]+ _0xbaa2[74]+ _0x69fexc[_0xbaa2[39]]+ _0xbaa2[75]+ _0x69fexc[_0xbaa2[40]]+ _0xbaa2[76]+ _0xbaa2[77]+ _0x69fexc[_0xbaa2[78]]+ _0xbaa2[79]+ _0x69fexc[_0xbaa2[78]]+ _0xbaa2[80]+ _0xbaa2[81]+ _0xbaa2[81], disableAutoPan: false, maxWidth: 0, pixelOffset: new google[_0xbaa2[15]].Size(10, 105), closeBoxMargin: _0xbaa2[9], closeBoxURL: _0xbaa2[82], isHidden: false, alignBottom: true, pane: _0xbaa2[83], enableEventPropagation: true
    }
    )
}

function onHtmlClick(_0x69fex13, key) {
    google[_0xbaa2[15]][_0xbaa2[49]][_0xbaa2[84]](markers[_0x69fex13][key], _0xbaa2[45])
}