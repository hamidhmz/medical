@extends('layouts.app')

@section('css')

@yield('d_css')

@endsection

@section('content')

	<main>
		<div id="breadcrumb">
			<div class="container">
				<ul>
					
					<li>صفحه ی مشاهده ی مقالات منتشر شده و ثبت نوبت و مشخصات پزشک</li>
				</ul>
			</div>
		</div>
		<!-- /breadcrumb -->
		
		<div class=" margin_60">
			<div class="row">

				<aside class="col-xl-3 col-lg-3 " id="sidebar">
					<div class="box_profile">
						<figure>
							<img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt="" class="img-fluid">
						</figure>
						<small>تخصص 1 - تخصص 2 </small>
						<h1>نام پزشک </h1>
						<span class="rating">
							<i class="icon_star voted"></i>
							<i class="icon_star voted"></i>
							<i class="icon_star voted"></i>
							<i class="icon_star voted"></i>
							<i class="icon_star"></i>
							<small>(145)</small>
							
						</span>
						<ul class="statistic">
							<li>854 بازدید</li>
							<li>124 بیماران</li>
						</ul>
						<ul class="contacts">
							<li><h6>آدرس :</h6> مشهد خیابان 1 کوچه 2 پلاک 3</li>
							<li><h6>تلفن </h6><a href="#!">051 54323342</a></li>
						</ul>
						
					</div>
				</aside>
				<!-- /asdide -->
				
				<div class="col-xl-7 col-lg-7 ">

					<div class="tabs_styled_2">
						<ul class="nav nav-tabs" role="tablist">
							
							<li class="nav-item">
								<a href="{{ url('/doctor/detail') }}" class="nav-link" >اطلاعات و آدرس پزشک </a>
							</li>
							<li class="nav-item">
								<a href="{{ url('/doctor/articles') }}" class="nav-link" >مقالات</a>
							</li>
							<li class="nav-item">
								<a href="{{ url('/doctor/FAQ') }}" class="nav-link" ">پرسش و پاسخ ها </a>
							</li>
							<li class="nav-item">
								<a href="{{ url('/doctor/ticket') }}" class="nav-link " >گرفتن نوبت</a>
							</li>
							<li class="nav-item">
								<a href="{{ url('/doctor/question') }}" class="nav-link " >پرسیدن سوال</a>
							</li>
						</ul>
						<!--/nav-tabs -->

						<div class="tab-content">
							

							@yield('d_content')

						</div>
						<!-- /tab-content -->
					</div>
					<!-- /tabs_styled -->
				</div>

				{{-- place for add,s --}}

				<div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
					
					<div class="">
						<div class="col-md-12">
							<div class="adds">

								<div class="items">
									<a href="" rel="nofolow">
										<img src="{{ asset('assets/img/adds/1.gif') }}">
									</a>
								</div>

								<div class="items">
									<a href="" rel="nofolow">
										<img src="{{ asset('assets/img/adds/2.gif') }}">
									</a>
								</div>

							</div>
						</div>
					</div>

				</div>

				{{-- .\ place for add,s --}}
				
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</main>
	<!-- /main -->

@endsection


@section('js')

	@yield('d_js')

	

@endsection