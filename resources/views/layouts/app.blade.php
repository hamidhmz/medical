<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Find easily a doctor and book online an appointment">
    <meta name="author" content="m0stafa_72">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ setting('site.title')  }}</title>

    <!-- Styles -->
    <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet">

     @yield('css')
</head>
<body>
   <div id="preloader" class="Fixed">
        <div data-loader="circle-side"></div>
    </div>
    <!-- /Preload-->
    <div id="page">

        <header class="header_sticky">  
            <a href="#menu" class="btn_mobile">
                <div class="hamburger hamburger--spin" id="hamburger">
                    <div class="hamburger-box">
                        <div class="hamburger-inner"></div>
                    </div>
                </div>
            </a>
            <!-- /btn_mobile-->
            <div class="container">
                <div class="row">
                    
                    <div class="col-lg-9 col-6">
                        <ul id="top_access">
                        <?php if(Auth::check()) {?>
                            <li><a href="{{ url('/user/dashboard') }}" data-toggle="tooltip" title="پنل کاربری من"><i class="pe-7s-user"></i></a></li>
                        <?php }else{?>
                            <li><a href="{{ url('/register') }}" data-toggle="tooltip" title="{{ __('voyager.navbar.home') }}"><i class="pe-7s-add-user"></i></a></li>
                        <?php } ?>
                        </ul>
                        <nav id="menu" class="main-menu">
                            <ul>
                                <li>
                                    <span><a href="{{ url('/') }}">{{ __('voyager.navbar.home') }} </a></span>
                                   
                                </li>
                                <li>
                                    <span><a href="{{ url('/') }}">پرسش و پاسخ </a></span>
                                   
                                </li>
                                <li>
                                    <span><a href="{{ url('/search/grid') }}">مراکز درمانی </a></span>
                                    <ul>
                                        <li><a href="{{ url('/search/list') }}">بیمارستان ها </a>
                                            
                                            <ul>
                                                <li><a target="_self" href="{{ url('/search/list') }}">{{ __('voyager.navbar.home') }}عمومی</a></li>
                                                <li><a target="_self" href="{{ url('/search/list') }}">قلب و عروق</a></li>
                                                <li><a target="_self" href="{{ url('/search/list') }}">ارتوپدی</a></li>
                                                <li><a target="_self" href="{{ url('/search/list') }}">مغزواعصاب</a></li>
                                                <li><a target="_self" href="{{ url('/search/grid') }}">چشم پزشکی</a></li>
                                                <li><a target="_self" href="{{ url('/search/grid') }}">تنفسی</a></li>
                                                <li><a target="_self" href="{{ url('/search/grid') }}">اورولوژی</a></li>
                                                <li><a target="_self" href="{{ url('/search/grid') }}">زنان و زایمان</a></li>
                                            </ul>

                                        </li>

                                        <li><a href="{{ url('/search/grid') }}">درمانگاه ها </a>
                                            <ul>
                                                <li><a target="_self" href="{{ url('/search/grid') }}">رادیولوژی و سونوگرافی</a></li>
                                                <li><a target="_self" href="{{ url('/search/grid') }}">مشاوره و روان درمان</a></li>
                                                <li><a target="_self" href="{{ url('/search/grid') }}">پاتولوژیست</a></li>
                                                <li><a target="_self" href="{{ url('/search/grid') }}">پوست و زیبایی</a></li>
                                                <li><a target="_self" href="{{ url('/search/grid') }}">دندان پزشکی</a></li>
                                                <li><a target="_self" href="{{ url('/search/grid') }}">چشم پزشکی</a></li>

                                            </ul>
                                        </li>

                                    </ul>
                                </li>
                                <li><span><a href="{{ url('/') }}">وبلاگ تخصصی ما</a></span></li>
                                <li><span><a href="{{ url('/') }}">قوانین ما</a></span></li>
                                <li><span><a href="{{ url('/') }}">پیشنهادات و شکایات</a></span></li>
                                <li><span><a href="{{ url('/') }}">درباره ما</a></span></li>
                                <li><span><a href="{{ url('/changelang/ar') }}">{{ __('voyager.locarization.arabic') }}</a></span></li>
                                <li><span><a href="{{ url('/changelang/fa') }}">{{ __('voyager.locarization.persian') }}</a></span></li>
                                <li><span><a href="{{ url('/changelang/en') }}">{{ __('voyager.locarization.english') }}</a></span></li>
                                <?php if (Auth::check()){ ?>
                                    <li>
                                        <span><a onclick="event.preventDefault();      document.getElementById('logout-form').submit();" href="{{ url('/') }}">{{ __('voyager.generic.logout') }} </a></span>
                                    </li>
                                <?php } ?>
                            </ul>
                        </nav>
                        <!-- /main-menu -->
                    </div>
                    <?php if (Auth::check()){ ?>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;" >
                            {{ csrf_field() }}
                        </form>
                    <?php } ?>
                    <div class="col-lg-3 col-6 ltr" >
                        <div id="logo_home">
                            <h6><a href="{{ url('/') }}" title="لوگوی سایت پزشکان">
                                <img src="{{ asset('assets/img/logo.png') }}">
                            </a></h6>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /container -->
        </header>
        <!-- /header -->
    

        @yield('content')




        <footer>
        <div class="container margin_60_35">
            <div class="row">
                <div class="col-lg-3 col-md-12">
                    <p>
                        <a href="{{ url('/') }}" title="Findoctor">
                            <img src="{{ asset('assets/img/logo.png')}}" data-retina="true" alt="" width="163" height="36" class="img-fluid">
                        </a>
                    </p>
                </div>
                <div class="col-lg-3 col-md-4">
                    <h5>دسترسی سریع</h5>
                    <ul class="links">
                        <li><a href="{{ url('/') }}">دصفحه اصلی</a></li>
                        <li><a href="{{ url('/') }}">درباره ما</a></li>
                        <li><a href="{{ url('/') }}">تماس با ما</a></li>
                        <li><a href="{{ url('/') }}">ورود و ثبت نام </a></li>
                        <li><a href="{{ url('/') }}">ثبت شکایات</a></li>
                        <li><a href="{{ url('/') }}">قوانین سایت</a></li>
                        <li><a href="{{ url('/') }}">تماس با ما </a></li>
                    </ul>
                </div>
                
                <div class="col-lg-3 col-md-4">
                    <h5>راه های ارتباطی ما </h5>
                    <ul class="contacts">
                        <li><a href="{{ url('/') }}"><i class="icon_mobile"></i> + 61 23 8093 3400</a></li>
                        <li><a href="{{ url('/') }}">email&#160;email.com</span></a></li>
                    </ul>
                    <div class="follow_us">
                        <h5>ما را دنبال کنید در :</h5>
                        <ul>
                            <li><a href="{{ url('/') }}" data-toggle="tooltip" title="فیس بوک"><i class="social_facebook"></i></a></li>
                            <li><a href="{{ url('/') }}" data-toggle="tooltip" title="توییتر"><i class="social_twitter"></i></a></li>
                            <li><a href="{{ url('/') }}" data-toggle="tooltip" title="تلگرام ما"><i class="social_linkedin"></i></a></li>
                            <li><a href="{{ url('/') }}" data-toggle="tooltip" title="اینستاگرام ما"><i class="social_instagram"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <h5>نماد اعتماد الکترونیکی</h5>
                    <ul class="links">
                        <li><p>نماد اعتماد الکترونیکی در دست انجام است </p></li>
                        
                    </ul>
                </div>
            </div>
            <!--/row-->
            <hr>
            <div class="">
               
                <div class=" text-center">
                    <div id="copy" class="margin-10">© 2017 تمامی حقوق مادی و معنوی محفوظ است</div>
                </div>
            </div>
        </div>
    </footer>
    <!--/footer-->
    </div>
    <!-- page -->

    <div id="toTop"></div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('assets/js/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ asset('assets/js/common_scripts.min.js') }}"></script>
    <script src="{{ asset('assets/js/functions.js') }}"></script>

     @yield('js')
</body>
</html>
