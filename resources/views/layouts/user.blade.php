@extends('layouts.app')

@section('css')



@yield('d_css')

@endsection

@section('content')

	<main>
		<div id="breadcrumb">
			<div class="container ">
				<ul>
					
					<li><strong>داشبورد کاربری </strong></li>
					<li  class="pull-left"><a href="" class="btn pumpkin" data-toggle="tooltip" title="خروج از حساب" style="margin-top: -18px;padding: 3px 6px"><i class="pe-7s-power" style="font-size: 21px"></i></a></li>
					<li></li>
				</ul>
			</div>
		</div>
		<!-- /breadcrumb -->
		
		<div class=" margin_60">
			<div class="row">
				<aside class="col-xl-3 col-lg-3 " id="sidebar">
					<div class="box_profile">
						<figure>
							<img src="{{ asset('assets/img/avatar3.jpg')}}" alt="" class="img-fluid">
						</figure>
						<h1>نام کاربر</h1>
						
						<ul class="row user-icons">
							<li><a href="{{ url('user/dashboard/setting') }}" class="" data-toggle="tooltip" title="ویرایش اطلاعات"><i class="pe-7s-config"></i></a></li>
							<li><a href="{{ url('user/questions') }}" class="" data-toggle="tooltip" title="پاسخ سوالات من "><i class="pe-7s-comment"></i></a></li>
							<li><a href="{{ url('user/ticket') }}" class="" data-toggle="tooltip" title="نوبت ویزیت من"><i class="pe-7s-plane"></i></a></li>
							<li><a href="{{ url('user/MyLeader') }}" class="" data-toggle="tooltip" title="لیدر من"><i class="pe-7s-user"></i></a></li>
							<li><a href="{{ url('user/records') }}" class="" data-toggle="tooltip" title="پرونده پزشکی من"><i class="pe-7s-note2"></i></a></li>

						</ul>
						<div class="clearfix"></div>
						<ul class="statistic">
							<li>اطلاعات شما تکمیل است</li>
							<li class="danger-light">اطلاعات شما ناقص است</li>
						</ul>
						<ul class="contacts ltr">
							<li><strong>تلفن :</strong><span class="ltr">  05154323342</span></li>
						</ul>
						<div class="text-center">
							<a href="{{ url('/user/dashboard/setting') }}" class="btn danger-light"> ویرایش اطلاعات </a></div>
					</div>

				</aside>
				<!-- /asdide -->
				
				<div class="col-xl-7 col-lg-7 ">

					<div class="tabs_styled_2">
						<ul class="nav nav-tabs" role="tablist">
							
							<li class="nav-item">
								<a href="{{ url('/user/dashboard') }}" class="nav-link" >اطلاعات و ویرایش</a>
							</li>
							<li class="nav-item">
								<a href="{{ url('/user/records') }}" class="nav-link" >پرونده پزشکی من</a>
							</li>
							<li class="nav-item">
								<a href="{{ url('/user/questions') }}" class="nav-link" ">پرسش و پاسخ</a>
							</li>
							<li class="nav-item">
								<a href="{{ url('/user/favorites') }}" class="nav-link" ">علاقه ها</a>
							</li>
							<li class="nav-item">
								<a href="{{ url('/user/ticket') }}" class="nav-link " > نوبت ویزیت من</a>
								
							</li>
							<li class="nav-item">
								<a href="{{ url('/user/Leaders') }}" class="nav-link " >لیدرها</a>
								
							</li>
							<li class="nav-item">
								<a href="{{ url('/user/MyLeader') }}" class="nav-link " >لیدر من</a>
							</li>
						</ul>
						<!--/nav-tabs -->

						<div class="tab-content">
							

							@yield('d_content')

						</div>
						<!-- /tab-content -->
					</div>
					<!-- /tabs_styled -->
				</div>

				{{-- place for add,s --}}

				<div class="col-lg-2 col-md-2 hidden-sm hidden-xs">
					
					<div class="">
						<div class="col-md-12">
							<div class="adds">

								<div class="items">
									<a href="" rel="nofolow">
										<img src="{{ asset('assets/img/adds/1.gif') }}">
									</a>
								</div>

								<div class="items">
									<a href="" rel="nofolow">
										<img src="{{ asset('assets/img/adds/2.gif') }}">
									</a>
								</div>

							</div>
						</div>
					</div>

				</div>

				{{-- .\ place for add,s --}}
				
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</main>
	<!-- /main -->

@endsection


@section('js')
	<script type="text/javascript" src="{{ asset('assets/js/progress.js') }}"></script>
	<script type="text/javascript">
		$('header').removeClass('header_sticky');
	    $('header').addClass('static');
	</script>
	@yield('d_js')

	

@endsection