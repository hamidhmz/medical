
@extends('layouts.app')

@section('css')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendor/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendor/jquery.bxslider.css') }}">
<style type="text/css">
	
	.select2.select2-container  {
		width: 250px !important;
	}

</style>
@endsection

@section('content')



<main>
		<div class="hero_home version_1">
			<div class="content">
				<h1>برای تشخیص ، درمان وپیشگیری با سایت اساتید پزشکی همراه شوید  </h1>
				<p class="margin-tb-20 red">
					سوالات خود را از متخصصین هر رشته بپرسید .
				</p>
				<p class="margin-tb-20 ">
					جدیدترین مقالات در هر رشته ی پزشکی را در سایت اساتید پزشکی جستجو کنید  .
				</p>
				
			</div>
		</div>
		<!-- /Hero -->
		{{-- <div class="my_bxslider">
			<ul class="bxslider">
			  <li><img src="{{ asset('assets/img/slider/1.jpg') }}" /></li>
			  <li><img src="{{ asset('assets/img/slider/2.jpg') }}" /></li>
			  <li><img src="{{ asset('assets/img/slider/3.jpg') }}" /></li>
			  <li><img src="{{ asset('assets/img/slider/4.jpg') }}" /></li>
			</ul>
			<div class="mask"></div>
			<div class="content">
				<h1 class="white">برای تشخیص ، درمان وپیشگیری با سایت اساتید پزشکی همراه شوید  </h1>
				<p class="margin-tb-20 white">
					سوالات خود را از متخصصین هر رشته بپرسید .
				</p>
				<p class="margin-tb-20 white">
					جدیدترین مقالات در هر رشته ی پزشکی را در سایت اساتید پزشکی جستجو کنید  .
				</p>
				
			</div>
		</div> --}}

		<div class="bg_color_1">
			<div class="container margin_120_95">
				<div class="main_title">
					<h2>آخرین مقالات و مطالب </h2>
					<p>آخرین مقالات و مطالب منتشر شده توسط پزشک ها را بخوانید </p>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<figure>

								<a href="{{url('/doctor/articles/slug')}}"><img src="{{ asset('assets/img/blog-4.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>دسته ی موضوعی </small>
								<h3>عنوان مقاله عنوان مقاله عنوان مقاله و عنوان مقاله و عنوان مقاله  </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/articles/slug')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>

					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<figure>

								<a href="{{url('/doctor/articles/slug')}}"><img src="{{ asset('assets/img/blog-5.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>دسته ی موضوعی </small>
								<h3>عنوان مقاله عنوان مقاله عنوان مقاله و عنوان مقاله و عنوان مقاله  </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/articles/slug')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<figure>

								<a href="{{url('/doctor/articles/slug')}}"><img src="{{ asset('assets/img/blog-6.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>دسته ی موضوعی </small>
								<h3>عنوان مقاله عنوان مقاله عنوان مقاله و عنوان مقاله و عنوان مقاله  </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/articles/slug')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<figure>

								<a href="{{url('/doctor/articles/slug')}}"><img src="{{ asset('assets/img/blog-6.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>دسته ی موضوعی </small>
								<h3>عنوان مقاله عنوان مقاله عنوان مقاله و عنوان مقاله و عنوان مقاله  </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/articles/slug')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<figure>

								<a href="{{url('/doctor/articles/slug')}}"><img src="{{ asset('assets/img/blog-4.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>دسته ی موضوعی </small>
								<h3>عنوان مقاله عنوان مقاله عنوان مقاله و عنوان مقاله و عنوان مقاله  </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/articles/slug')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<figure>

								<a href="{{url('/doctor/articles/slug')}}"><img src="{{ asset('assets/img/blog-5.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>دسته ی موضوعی </small>
								<h3>عنوان مقاله عنوان مقاله عنوان مقاله و عنوان مقاله و عنوان مقاله  </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/articles/slug')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>
					
				</div>
				<!-- /row -->
				<p class="text-center add_top_30"><a href="{{url('/articles')}}" class="btn_1 medium">مشاهده ی تمام مقالات</a></p>
			</div>
			<!-- /container -->
		</div>
		<!-- /white_bg -->

		<div class="container margin_90_75 padding-top-25">
			<div class="main_title">
				<h2>یافتن پزشک و کلینیک براساس تخصص ها</h2>
				<p>میتوانید پزشک مورد نظر خود را براساس تخصص جستجو نمایید و سپس در صفحه بعد از <strong>فلتر شهر</strong> استفاده نمایید .</p>
			</div>
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<a href="{{url('/search/list')}}" class="box_cat_home">
						<i class="icon-info-4"></i>
						<img src="{{ asset('assets/img/icons/icon_cat_1.svg') }}" width="60" height="60" alt="">
						<h3>مراقبت های اولیه</h3>
						<ul class="clearfix">
							<li><strong>124</strong>پزشک</li>
							<li><strong>60</strong>درمانگاه</li>
						</ul>
					</a>
				</div>
				<div class="col-lg-3 col-md-6">
					<a href="{{url('/search/list')}}" class="box_cat_home">
						<i class="icon-info-4"></i>
						<img src="{{ asset('assets/img/icons/icon_cat_2.svg') }}" width="60" height="60" alt="">
						<h3>قلب و عروق</h3>
						<ul class="clearfix">
							<li><strong>124</strong>پزشک</li>
							<li><strong>60</strong>درمانگاه</li>
						</ul>
					</a>
				</div>
				<div class="col-lg-3 col-md-6">
					<a href="{{url('/search/list')}}" class="box_cat_home">
						<i class="icon-info-4"></i>
						<img src="{{ asset('assets/img/icons/icon_cat_3.svg') }}" width="60" height="60" alt="">
						<h3>رزونانس MRI</h3>
						<ul class="clearfix">
							<li><strong>124</strong>پزشک</li>
							<li><strong>60</strong>درمانگاه</li>
						</ul>
					</a>
				</div>
				<div class="col-lg-3 col-md-6">
					<a href="{{url('/search/list')}}" class="box_cat_home">
						<i class="icon-info-4"></i>
						<img src="{{ asset('assets/img/icons/icon_cat_4.svg') }}" width="60" height="60" alt="">
						<h3>آزمایش خون</h3>
						<ul class="clearfix">
							<li><strong>124</strong>پزشک</li>
							<li><strong>60</strong>درمانگاه</li>
						</ul>
					</a>
				</div>
				<div class="col-lg-3 col-md-6">
					<a href="{{url('/search/list')}}" class="box_cat_home">
						<i class="icon-info-4"></i>
						<img src="{{ asset('assets/img/icons/icon_cat_7.svg') }}" width="60" height="60" alt="">
						<h3>آزمایشگاه</h3>
						<ul class="clearfix">
							<li><strong>124</strong>پزشک</li>
							<li><strong>60</strong>درمانگاه</li>
						</ul>
					</a>
				</div>
				<div class="col-lg-3 col-md-6">
					<a href="{{url('/search/list')}}" class="box_cat_home">
						<i class="icon-info-4"></i>
						<img src="{{ asset('assets/img/icons/icon_cat_5.svg') }}" width="60" height="60" alt="">
						<h3>دندانپزشکی</h3>
						<ul class="clearfix">
							<li><strong>124</strong>پزشک</li>
							<li><strong>60</strong>درمانگاه</li>
						</ul>
					</a>
				</div>
				<div class="col-lg-3 col-md-6">
					<a href="{{url('/search/list')}}" class="box_cat_home">
						<i class="icon-info-4"></i>
						<img src="{{ asset('assets/img/icons/icon_cat_6.svg') }}" width="60" height="60" alt="">
						<h3>اشعه ایکس</h3>
						<ul class="clearfix">
							<li><strong>124</strong>پزشک</li>
							<li><strong>60</strong>درمانگاه</li>
						</ul>
					</a>
				</div>
				<div class="col-lg-3 col-md-6">
					<a href="{{url('/search/list')}}" class="box_cat_home">
						<i class="icon-info-4"></i>
						<img src="{{ asset('assets/img/icons/icon_cat_8.svg') }}" width="60" height="60" alt="">
						<h3>روانشناس</h3>
						<ul class="clearfix">
							<li><strong>124</strong>پزشک</li>
							<li><strong>60</strong>درمانگاه</li>
						</ul>
					</a>
				</div>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->

		<div class="bg_color_1">
			<div class="container margin_120_95">
				<div class="main_title">
					<h2>برخی از برترین پزشکان سایت  </h2>
					<p>پزشکانی که توسط کاربران امتایز بیشتری گرفته اند را ببینید </p>
				</div>
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<span class="why_the_best">برترین مطلب</span>
							<figure>

								<a href="{{url('/doctor/detail')}}"><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>تخصص پزشک</small>
								<h3>نام دکتر </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/detail')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<span class="why_the_best">بیشترین مطلب</span>
							<figure>

								<a href="{{url('/doctor/detail')}}"><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>تخصص پزشک</small>
								<h3>نام دکتر </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/detail')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<span class="why_the_best">بیشترین بازدید</span>
							<figure>

								<a href="{{url('/doctor/detail')}}"><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>تخصص پزشک</small>
								<h3>نام دکتر </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/detail')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<span class="why_the_best">بیشترین بازدید</span>
							<figure>

								<a href="{{url('/doctor/detail')}}"><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>تخصص پزشک</small>
								<h3>نام دکتر </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/detail')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<span class="why_the_best">بیشترین پاسخ</span>
							<figure>

								<a href="{{url('/doctor/detail')}}"><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>تخصص پزشک</small>
								<h3>نام دکتر </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/detail')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="box_list home">
							<a href="#0" data-toggle="tooltip" data-placement="top" title="افزودن به علاقه ها" class="wish_bt"></a>
							<span class="why_the_best">برترین مطلب</span>
							<figure>

								<a href="{{url('/doctor/detail')}}"><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" class="img-fluid" alt=""></a>
								<div class="preview"><span>ادامه مطلب</span></div>
							</figure>
							<div class="wrapper">
								<small>تخصص پزشک</small>
								<h3>نام دکتر </h3>
								<p>توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله توضیح یک خطی از مقاله </p>
								<span class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i> <small>(145)</small></span>
							</div>
							<ul>
								<li><i class="icon-eye-7"></i> 854 دیده شده</li>
								<li><a href="{{url('/doctor/detail')}}">مطالعه بیشتر </a></li>
							</ul>
						</div>
					</div>

					
				</div>
				<!-- /row -->
				<p class="text-center add_top_30"><a href="{{url('/')}}" class="btn_1 medium">مشاهده ی برترین پزشکان</a></p>
			</div>
			<!-- /container -->
		</div>
		<!-- /white_bg -->

		<div class="container margin_120_95">
			<div class="main_title">
				<h2>یافتن سریع پزشک یا درمانگاه </h2>
				<p>پزشک یا درمانگاه ها را براساس نوع تخصص یا موقیت مکانی جستجو کنید .</p>
			</div>
			<div class="row justify-content-center">
				<div class="col-xl-4 col-lg-5 col-md-6">
					<div class="list_home">
						<div class="list_title">
							<i class="icon_pin_alt"></i>
							<h3>جستجو براساس شهر </h3>
						</div>
						<ul>
							<li><a href="{{ url('/doctor/detail') }}"><strong>23</strong>تهران</a></li>
							<li><a href="{{ url('/doctor/detail') }}"><strong>23</strong>مشهد</a></li>
							<li><a href="{{ url('/doctor/detail') }}"><strong>23</strong>اصفحان</a></li>
							<li><a href="{{ url('/doctor/detail') }}"><strong>23</strong>تبریز</a></li>
							<li><a href="{{ url('/doctor/detail') }}"><strong>23</strong>شیراز</a></li>
							<li><a href="{{ url('/doctor/detail') }}"><strong>23</strong>بوشهر</a></li>
							<li><a href="{{ url('/doctor/detail') }}"><strong>23</strong>کرمان</a></li>
							<li><a href="{{ url('/doctor/detail') }}"><strong>23</strong>گرگان</a></li>
							<li><a href="{{ url('/doctor/detail') }}"><strong>23</strong>بجنورد</a></li>
							<li><a href="{{ url('/search/list') }}">بیشتر ...</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xl-4 col-lg-5 col-md-6">
					<div class="list_home">
						<div class="list_title">
							<i class="icon_archive_alt"></i>
							<h3>جستجو براساس نوع تخصص </h3>
						</div>
						<ul>
							<li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> متخصص بیماری های کودکان</a></li>
							<li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> متخصص ایمپلنت - پروتز دندان</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> متخصص جراحی لثه</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> جراح دهان و فک و صورت</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> متخصص ارتودنسی</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> متخصص دندانپزشکی اطفال</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> تغذیه</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> روانشناس</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> مشاوره</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> روانکاو</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> ژنتیک</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> ارتوپدی فنی</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> کایروپراکتیک</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> پرستار</a></li>
					        <li><a href="{{ url('/doctor/detail') }}"><strong>23</strong> سمعک و شنوایی سنجی</a></li>
							
							<li><a href="{{ url('/search/list') }}">بیشتر ....</a></li>
						</ul>
					</div>
				</div>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->

		
		<!-- /app_section -->
	</main>




@endsection



@section('js')

<script type="text/javascript" src="{{ asset('assets/js/jquery.bxslider.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/select2.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
	    $('.js-example-basic-single').select2();


	    $(document).ready(function(){
		  $('.bxslider').bxSlider();
		});

	});
</script>
@endsection