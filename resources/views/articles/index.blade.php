@extends('layouts.app')

@section('css')

<style type="text/css">
	
	article.blog figure img , article.blog figure {height: 250px !important}

</style>

@endsection

@section('content')

	<main>
		<div id="breadcrumb">
			<div class="container">
				<ul>
					
					<li>صفحه ی مشاهده ی تمام مقالات منتشر شده </li>
				</ul>
			</div>
		</div>
		<!-- /breadcrumb -->
		
		<div class="container margin_60">
			<div class="main_title">
				<h1>مرجع تخصصی مقالات پزشکی </h1>
				<p>جدیدترین مقالات در هر رشته ی پزشکی را در سایت اساتید پزشکی جستجو کنید .</p>
			</div>
			<div class="row">

				<aside class="col-xl-3 col-md-4 bg-white" id="sidebar">
					<div class="widget">
						<form>
							<div class="form-group">
								<input type="text" name="search" id="search" class="form-control bg-silver-light margin-top-10" placeholder="عبارت خود را جسجو کنید .. ">
							</div>
							<button type="submit" id="submit" class="btn_1"> جستجو </button>
						</form>
					</div>

					<div class="widget">
						<div class="widget-title">
							<h4>براساس دسته بندی </h4>
						</div>
						<ul class="cats">
							<li><a href="#">دسته بندی 1 <span>(12)</span></a></li>
							<li><a href="#">دسته بندی 2 <span>(21)</span></a></li>
							<li class="active"><a href="#">دسته بندی 3 <span>(44)</span></a></li>
							<li><a href="#">دسته بندی 4 <span>(09)</span></a></li>
							<li><a href="#">دسته بندی 5 <span>(31)</span></a></li>
							<li><a href="#">دسته بندی 6 <span>(31)</span></a></li>
							<li><a href="#">دسته بندی 7 <span>(31)</span></a></li>
							<li><a href="#">دسته بندی 8 <span>(31)</span></a></li>
							<li><a href="#">دسته بندی 9 <span>(31)</span></a></li>
							<li><a href="#">دسته بندی 10 <span>(31)</span></a></li>
							<li><a href="#">دسته بندی 11 <span>(31)</span></a></li>
						</ul>
					</div>
					<!-- /widget -->
				</aside>
				<!-- /asdide -->
				
				<div class="col-xl-9 col-md-8">

					<article class="blog wow fadeIn">
						<div class="row no-gutters">
							<div class="col-md-5">
								<figure>
									<a href="{{ url('doctor/articles/slug') }}">
										<img src="{{ asset('assets/img/blog-1.jpg')}}" alt="">
										<div class="preview">
											<span>ادامه مطلب </span>
										</div>
									</a>
								</figure>
							</div>
							<div class="col-md-7">
								<div class="post_info">
									<small>20 / 10 / 96</small>
									<h3 class="text-limit-line"><a href="">عنوان مقاله عنوان مقاله </a></h3>
									<p>ین یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد .</p>
									<ul>
										<li>
											<a href="{{ url('doctor/articles/slug') }}" class="btn danger-light">ادامه مطلب</a>
										</li>
										<li class="pull-left"><i class="icon_comment_alt"></i> 20</li>
									</ul>
								</div>
							</div>
						</div>
					</article>

					<!-- /article -->

					<article class="blog wow fadeIn">
						<div class="row no-gutters">
							<div class="col-md-5">
								<figure>
									<a href="{{ url('doctor/articles/slug') }}">
										<img src="{{ asset('assets/img/blog-6.jpg')}}" alt="">
										<div class="preview">
											<span>ادامه مطلب </span>
										</div>
									</a>
								</figure>
							</div>
							<div class="col-md-7">
								<div class="post_info">
									<small>20 / 10 / 96</small>
									<h3 class="text-limit-line"><a href="">عنوان مقاله عنوان مقاله </a></h3>
									<p>ین یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد .</p>
									<ul>
										<li>
											<a href="{{ url('doctor/articles/slug') }}" class="btn danger-light">ادامه مطلب</a>
										</li>
										<li class="pull-left"><i class="icon_comment_alt"></i> 20</li>
									</ul>
								</div>
							</div>
						</div>
					</article>
					<article class="blog wow fadeIn">
						<div class="row no-gutters">
							<div class="col-md-5">
								<figure>
									<a href="{{ url('doctor/articles/slug') }}">
										<img src="{{ asset('assets/img/blog-5.jpg')}}" alt="">
										<div class="preview">
											<span>ادامه مطلب </span>
										</div>
									</a>
								</figure>
							</div>
							<div class="col-md-7">
								<div class="post_info">
									<small>20 / 10 / 96</small>
									<h3 class="text-limit-line"><a href="">عنوان مقاله عنوان مقاله </a></h3>
									<p>ین یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد .</p>
									<ul>
										<li>
											<a href="{{ url('doctor/articles/slug') }}" class="btn danger-light">ادامه مطلب</a>
										</li>
										<li class="pull-left"><i class="icon_comment_alt"></i> 20</li>
									</ul>
								</div>
							</div>
						</div>
					</article>
					<article class="blog wow fadeIn">
						<div class="row no-gutters">
							<div class="col-md-5">
								<figure>
									<a href="{{ url('doctor/articles/slug') }}">
										<img src="{{ asset('assets/img/blog-2.jpg')}}" alt="">
										<div class="preview">
											<span>ادامه مطلب </span>
										</div>
									</a>
								</figure>
							</div>
							<div class="col-md-7">
								<div class="post_info">
									<small>20 / 10 / 96</small>
									<h3 class="text-limit-line"><a href="">عنوان مقاله عنوان مقاله </a></h3>
									<p>ین یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد .</p>
									<ul>
										<li>
											<a href="{{ url('doctor/articles/slug') }}" class="btn danger-light">ادامه مطلب</a>
										</li>
										<li class="pull-left"><i class="icon_comment_alt"></i> 20</li>
									</ul>
								</div>
							</div>
						</div>
					</article>
					<article class="blog wow fadeIn">
						<div class="row no-gutters">
							<div class="col-md-5">
								<figure>
									<a href="{{ url('doctor/articles/slug') }}">
										<img src="{{ asset('assets/img/blog-3.jpg')}}" alt="">
										<div class="preview">
											<span>ادامه مطلب </span>
										</div>
									</a>
								</figure>
							</div>
							<div class="col-md-7">
								<div class="post_info">
									<small>20 / 10 / 96</small>
									<h3 class="text-limit-line"><a href="">عنوان مقاله عنوان مقاله </a></h3>
									<p>ین یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد .</p>
									<ul>
										<li>
											<a href="{{ url('doctor/articles/slug') }}" class="btn danger-light">ادامه مطلب</a>
										</li>
										<li class="pull-left"><i class="icon_comment_alt"></i> 20</li>
									</ul>
								</div>
							</div>
						</div>
					</article>
					<article class="blog wow fadeIn">
						<div class="row no-gutters">
							<div class="col-md-5">
								<figure>
									<a href="{{ url('doctor/articles/slug') }}">
										<img src="{{ asset('assets/img/blog-4.jpg')}}" alt="">
										<div class="preview">
											<span>ادامه مطلب </span>
										</div>
									</a>
								</figure>
							</div>
							<div class="col-md-7">
								<div class="post_info">
									<small>20 / 10 / 96</small>
									<h3 class="text-limit-line"><a href="">عنوان مقاله عنوان مقاله </a></h3>
									<p>ین یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد .</p>
									<ul>
										<li>
											<a href="{{ url('doctor/articles/slug') }}" class="btn danger-light">ادامه مطلب</a>
										</li>
										<li class="pull-left"><i class="icon_comment_alt"></i> 20</li>
									</ul>
								</div>
							</div>
						</div>
					</article>

				</div>

				
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</main>
	<!-- /main -->

@endsection


@section('js')

	

@endsection