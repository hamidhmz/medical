
@extends('layouts.user')


@section('d_css')


@endsection

@section('d_content')




<div class="tab-pane fade show active" >

	<div class="row">
		<strong class="red margin-tb-20">توجه : درصورت نیاز به مترجم میتوانید از قسمت لیدر ها مترجم یا کارگزار انتخاب نمایید .</strong>
	</div>
	<div class="row">
		
		<div class="col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat bg-blue">
                <div class="visual">
                    <i class="pe-7s-like"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span >134</span>
                    </div>
                    <div class="desc">  علاقه های من </div>
                </div>
                <a class="more bg-navy white" href=""> مشاهده همه
                    <i class="arrow_left pull-left"></i>
                </a>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat bg-orange">
                <div class="visual">
                    <i class="pe-7s-like2"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span > 1 </span>
                    </div>
                    <div class="desc"> ویزیت فعال </div>
                </div>
                <a class="more bg-navy white" href=""> مشاهده همه
                    <i class="arrow_left pull-left"></i>
                </a>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat bg-purple">
                <div class="visual">
                    <i class="pe-7s-add-user"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span > 1 </span>
                    </div>
                    <div class="desc"> لیدر من  </div>
                </div>
                <a class="more bg-navy white" href=""> مشاهده همه
                    <i class="arrow_left pull-left"></i>
                </a>
            </div>
        </div>

	</div>

	<div class="main_title_4">
		<h3><i class="icon_circle-slelected"></i> اطلاعات شخصی شما </h3>
	</div>
	
	
	<div class="indent_title_in">
		<i class="pe-7s-user"></i>
		<h3>اطلاعات شخصی  </h3>
		
	</div>
	<div class="wrapper_indent">
		<div class="row">
			<div class="col-md-5">
				<ul class="bullets">
					<li><strong>نام و نام خانوادگی :</strong> <small></small> </li>
					<li><strong>کد ملی :</strong> <small></small> </li>
					<li><strong>جنسیت :</strong> <small></small> </li>
					<li><strong>سال تولد :</strong> <small></small>  </li>
				</ul>
			</div>
			<div class="col-md-5">
				<ul class="bullets">
					<li><strong>نام پدر :</strong> <small></small> </li>
					<li><strong>شماره تماس ثابت  :</strong> <small></small> </li>
					<li><strong>شماره همراه  :</strong> <small></small> </li>
					<li><strong>ایمیل :</strong> <small></small>  </li>
				</ul>
			</div>
			
		</div>
		<!-- /row-->
		<div class="col-md-12">
			<div class="margin-tb-20">
				<strong>توضیحات تکمیلی</strong>				
			</div>
			<div>
				<p>توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است </p>
			</div>
		</div>
	</div>
	<!--  End wrapper indent -->
	

</div>




@endsection


@section('d_js')

<script type="text/javascript">
	$('.tabs_styled_2 ul li:nth-child(1) a').addClass('active');
</script>

@endsection