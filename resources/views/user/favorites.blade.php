@extends('layouts.user')



@section('d_css')

<style type="text/css">
	.box_list figure {
		height: 150px
	}
	.box_list .wrapper h3 {
		overflow: hidden;
	    text-overflow: ellipsis;
	    white-space: nowrap;
	    width: 95%;
	    font-size: 13px;
	}
	.box_list .wrapper p {
		font-size: 11px;
	}
</style>
@endsection

@section('d_content')


<div class="tab-pane fade show active" >


	<div class="dis-block bg-red padding-top-20 white padding-bottom-15 margin-tb-20 text-center">
		<p>کاربر گرامی شما هیچ علاقه مندی در لیست ندارید . </p>
	</div>

	<div class="main_title_4">
		<h3><i class="icon_circle-slelected"></i> شما 12 مورد به علاقه های خود افزوده اید . </h3>
	</div>

	<div class="row">
		
		<div class="col-md-4">
			<div class="box_list wow fadeIn">
				<a href="#0" class="wish_bt"> پزشک </a>
				<figure>
					<a href="{{url('/doctor/detail')}}"><img src="{{ asset('assets/img/doctor_listing_2.jpg')}}" class="img-fluid" alt="">
						<div class="preview"><span>مطالعه بیشتر</span></div>
					</a>
				</figure>
				<div class="wrapper">
					<small>تخصص</small>
					<h3>نام پزشک </h3>
					<p>یکی دو خط توضیح پزشک یکی دو خط توضیح پزشک  یکی دو خط توضیح پزشک  یکی دو خط توضیح پزشک....</p>
					<span class="rating">
						<i class="icon_star voted"></i>
						<i class="icon_star voted"></i>
						<i class="icon_star voted"></i>
						<i class="icon_star"></i>
						<i class="icon_star"></i> 
						<small>(145)</small>
					</span>
					
				</div>
				<ul>
					<li class="clearfix"></li>
					<li class="clearfix"></li>
					<li><a href="{{url('/doctor/detail')}}">مشاهد و نوبت گیری</a></li>
				</ul>
			</div>
		</div>

		<div class="col-md-4">
			<div class="box_list wow fadeIn">
				<a href="#0" class="wish_bt"> مقاله </a>
				<figure>
					<a href="{{url('/doctor/articles')}}"><img src="{{ asset('assets/img/blog-2.jpg')}}" class="img-fluid" alt="">
						<div class="preview"><span>مطالعه بیشتر</span></div>
					</a>
				</figure>
				<div class="wrapper">
					<small>دسته موضوعی</small>
					<h3>عنوان مطلب عنوان مطلب عنوان مطلب عنوان مطلب </h3>
					<p>یکی دو خط توضیح پزشک یکی دو خط توضیح پزشک  یکی دو خط توضیح پزشک  یکی دو خط توضیح پزشک....</p>
					<span class="rating">
						<i class="icon_star voted"></i>
						<i class="icon_star voted"></i>
						<i class="icon_star voted"></i>
						<i class="icon_star"></i>
						<i class="icon_star"></i> 
						<small>(145)</small>
					</span>
					
				</div>
				<ul>
					<li class="clearfix"></li>
					<li class="clearfix"></li>
					<li><a href="{{url('/doctor/articles')}}">ادامه مطلب</a></li>
				</ul>
			</div>
		</div>

		<div class="col-md-4">
			<div class="box_list wow fadeIn">
				<a href="#0" class="wish_bt"> مقاله </a>
				<figure>
					<a href="{{url('/doctor/articles')}}"><img src="{{ asset('assets/img/blog-3.jpg')}}" class="img-fluid" alt="">
						<div class="preview"><span>مطالعه بیشتر</span></div>
					</a>
				</figure>
				<div class="wrapper">
					<small>دسته موضوعی</small>
					<h3>عنوان مطلب عنوان مطلب عنوان مطلب عنوان مطلب </h3>
					<p>یکی دو خط توضیح پزشک یکی دو خط توضیح پزشک  یکی دو خط توضیح پزشک  یکی دو خط توضیح پزشک....</p>
					<span class="rating">
						<i class="icon_star voted"></i>
						<i class="icon_star voted"></i>
						<i class="icon_star voted"></i>
						<i class="icon_star"></i>
						<i class="icon_star"></i> 
						<small>(145)</small>
					</span>
					
				</div>
				<ul>
					<li class="clearfix"></li>
					<li class="clearfix"></li>
					<li><a href="{{url('/doctor/articles')}}">ادامه مطلب</a></li>
				</ul>
			</div>
		</div>


	</div>


</div>




@endsection


@section('d_js')

<script type="text/javascript">
	$('.tabs_styled_2 ul li:nth-child(4) a').addClass('active');
</script>

@endsection