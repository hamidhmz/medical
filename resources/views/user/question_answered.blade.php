

@extends('layouts.user')

@section('d_css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendor/sweetalert2.css') }}">

<style type="text/css">
	
	.strip_list {
		border: 1px solid #ccc;
	}

</style>

@endsection

@section('d_content')




<!-- /tab_2 -->

<div class="tab-pane fade show active" >

	<div class="dis-block bg-red padding-top-20 white padding-bottom-15 margin-tb-20 text-center">
		<p>کاربر گرامی شما هیچ سوالی در لیست ندارید . </p>
	</div>

	<div class="tabs_styled_3">
		<ul class="nav nav-tabs" role="tablist">
			
			<li class="">
				<a href="{{ url('/user/questions') }}" class="nav-link " >سوالات پاسخ داده نشده</a>
			</li>
			<li class="">
				<a href="{{ url('/user/questions/answered') }}" class="nav-link active" >سوالات پاسخ داده شده </a>
			</li>
			
		</ul>
		<!--/nav-tabs -->

		<div class="tab-content">
			<div class="main_title_4">
				<h3><i class="icon_circle-slelected"></i> 13 سوال شما پاسخ داده شده است</h3>
			</div>

			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام پزشک مربوطه</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li><a href="#0"  class="btn_listing">&nbsp;</a></li>
					<li><a href="#!" onclick="answer('اول باید بای ایجکس مقدار پاخ به این سوال گرفته بشه بعد توی یه مقیر ریخته شه بعد بزای توی قسمت خودش ' , 2)">مشاهده پاسخ</a></li>
				</ul>
			</div>

			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام پزشک مربوطه</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li><a href="#0"  class="btn_listing">&nbsp;</a></li>
					<li><a href="#!" onclick="answer('اول باید بای ایجکس مقدار پاخ به این سوال گرفته بشه بعد توی یه مقیر ریخته شه بعد بزای توی قسمت خودش ' , 2)">مشاهده پاسخ</a></li>
				</ul>
			</div>
			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام پزشک مربوطه</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li><a href="#0"  class="btn_listing">&nbsp;</a></li>
					<li><a href="#!" onclick="answer('اول باید بای ایجکس مقدار پاخ به این سوال گرفته بشه بعد توی یه مقیر ریخته شه بعد بزای توی قسمت خودش ' , 2)">مشاهده پاسخ</a></li>
				</ul>
			</div>
			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام پزشک مربوطه</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li><a href="#0"  class="btn_listing">&nbsp;</a></li>
					<li><a href="#!" onclick="answer('اول باید بای ایجکس مقدار پاخ به این سوال گرفته بشه بعد توی یه مقیر ریخته شه بعد بزای توی قسمت خودش ' , 2)">مشاهده پاسخ</a></li>
				</ul>
			</div>
			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام پزشک مربوطه</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li><a href="#0"  class="btn_listing">&nbsp;</a></li>
					<li><a href="#!" onclick="answer('اول باید بای ایجکس مقدار پاخ به این سوال گرفته بشه بعد توی یه مقیر ریخته شه بعد بزای توی قسمت خودش ' , 2)">مشاهده پاسخ</a></li>
				</ul>
			</div>
			
			<!-- End review-container -->
		</div>
	</div>
</div>




@endsection


@section('d_js')


<script type="text/javascript" src="{{ asset('assets/js/sweetalert2.js') }}"></script>

<script type="text/javascript">
	$('.tabs_styled_2 ul li.nav-item:nth-child(3) a').addClass('active');

	function answer(argument , int) {
		// body...

		swal({
		  html : 
		  '<div class="">' +
				'<div class=" row">' +
					'<strong class="margin-tb-5 teal" style="font-size: 21px">متن سوال </strong>' +
					'<p class="text-right padding-5" style="font-size: 12px">'+argument+'</p>' +
				'</div>' +
				'<hr>' +
				'<div class=" row">' +
					'<strong class="margin-tb-5 teal" style="font-size: 21px">متن پاسخ پزشک </strong>' +
					'<div class="col-lg-12">' +
						'<div class="form-group">' +
							'<p class="text-right padding-5" style="font-size: 12px">'+argument+'</p>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>' ,

		  
		  // input: 'email',
		  showCancelButton: true,
		  showConfirmButton: false,
		  
		  allowOutsideClick: false
		})

	// 	preConfirm: function () {
	//     return new Promise(function (resolve, reject) {
	        
	//         $.ajax({
	//             url: '/User/BadVote',
	//             type: 'POST',
	//             datatype: 'json',
	//             data: { id:tid }


	//         }).done(function (data) {
	//             if (data) {
	//                 resolve()
	                
	//             }
	           
	//         })
	     
	//     })
	//   },
	//   allowOutsideClick: false
	// }).then(function (argument) {
	//   swal({
	//     type: 'success',
	//     text: 'با موفقیت ثبت شد توسط تیم پشتیبانی بررسی خواهد شد '
	//   })
	// })

	}


</script>
@endsection