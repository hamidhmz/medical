@extends('layouts.user')



@section('d_css')

<style type="text/css">
	.strip_list {
		border: 1px solid #ccc;
	}
</style>

@endsection

@section('d_content')




<div class="tab-pane fade show active" >

	<div class="dis-block bg-red padding-top-20 white padding-bottom-15 margin-tb-20 text-center">
		<p>کاربر گرامی شما هیچ سوالی در لیست ندارید . </p>
	</div>

	<div class="tabs_styled_3">
		<ul class="nav nav-tabs" role="tablist">
			
			<li class="">
				<a href="{{ url('/user/questions') }}" class="nav-link active" >سوالات پاسخ داده نشده</a>
			</li>
			<li class="">
				<a href="{{ url('/user/questions/answered') }}" class="nav-link " >سوالات پاسخ داده شده </a>
			</li>
			
		</ul>
		<!--/nav-tabs -->

		<div class="tab-content">
			<div class="main_title_4">
				<h3><i class="icon_circle-slelected"></i> شما 13 سوال جدید پاسخ داده نشده دارید </h3>
			</div>

			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام پزشک مربوطه</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li class="red">هنوز پاسخ داده نشده</li>

				</ul>
			</div>

			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام پزشک مربوطه</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li class="red">هنوز پاسخ داده نشده</li>

				</ul>
			</div>

			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام پزشک مربوطه</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li class="red">هنوز پاسخ داده نشده</li>

				</ul>
			</div>

			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام پزشک مربوطه</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li class="red">هنوز پاسخ داده نشده</li>

				</ul>
			</div>

			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام پزشک مربوطه</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li class="red">هنوز پاسخ داده نشده</li>

				</ul>
			</div>

			
			<!-- End review-container -->
		</div>
	</div>
</div>







@endsection


@section('d_js')

<script type="text/javascript">
	$('.tabs_styled_2 ul li:nth-child(3) a').addClass('active');
</script>

@endsection