@extends('layouts.user')



@section('d_css')


@endsection

@section('d_content')




<div class="tab-pane fade show active" >

	<div class="dis-block bg-red padding-top-20 white padding-bottom-15 margin-tb-20 text-center">
		<p>کاربر گرامی شما هیچ نوبت ویزیتی در لیست ندارید . </p>
	</div>

	<div class="main_title_4">
		<h3><i class="icon_circle-slelected"></i> شما 1 نوبت رزرو شده دارید  . </h3>
	</div>


	<div class="indent_title_in">
		<i class="pe-7s-alarm"></i>
		<h3>اطلاعات نوبت  </h3>
		
	</div>

	<div class="wrapper_indent">
		<div class="row">
			<div class="col-md-5">
				<ul class="bullets">
					<li><strong>ساعت ویزیت :</strong> <small></small> </li>
					<li><strong>تاریخ :</strong> <small></small> </li>
					<li><strong>آدرس مطب :</strong> <small></small> </li>
					<li><strong>شماره تماس  :</strong> <small></small>  </li>
				</ul>
			</div>
			
		</div>
		<!-- /row-->
	</div>
	<!--  End wrapper indent -->


	<div class="indent_title_in">
		<i class="pe-7s-user"></i>
		<h3>اطلاعات بیمار  </h3>
		
	</div>

	<div class="wrapper_indent">
		<div class="row">
			<div class="col-md-5">
				<ul class="bullets">
					<li><strong>نام و نام خانوادگی :</strong> <small></small> </li>
					<li><strong>کد ملی :</strong> <small></small> </li>
					<li><strong>جنسیت :</strong> <small></small> </li>
					<li><strong>سال تولد :</strong> <small></small>  </li>
				</ul>
			</div>
			<div class="col-md-5">
				<ul class="bullets">
					<li><strong>نام پدر :</strong> <small></small> </li>
					<li><strong>شماره تماس ثابت  :</strong> <small></small> </li>
					<li><strong>شماره همراه  :</strong> <small></small> </li>
					<li><strong>ایمیل :</strong> <small></small>  </li>
				</ul>
			</div>
			
		</div>
		<!-- /row-->
	</div>
	<!--  End wrapper indent -->

	<div class="indent_title_in">
		<i class="pe-7s-user"></i>
		<h3>اطلاعات پزشک  </h3>
		
	</div>
	<div class="wrapper_indent">
		<div class="row">
			<div class="col-md-5">
				<ul class="bullets">
					<li><strong>نام و نام خانوادگی :</strong> <small></small> </li>
					<li><strong>تخصص :</strong> <small></small> </li>
					<li><strong>جنسیت :</strong> <small></small> </li>
					<li><strong>سال تولد :</strong> <small></small>  </li>
				</ul>
			</div>
			<div class="col-md-5">
				<ul class="bullets">
					<li><strong>استان :</strong> <small></small> </li>
					<li><strong>شهر  :</strong> <small></small> </li>
					<li><strong>آدرس مطب :</strong> <small></small> </li>
					<li><strong>شماره تماس  :</strong> <small></small>  </li>
				</ul>
			</div>
			
		</div>
		<!-- /row-->
	</div>
	<!--  End wrapper indent -->
	<div class="col-md-12">
		<div class="margin-tb-20">
			<strong>توضیحات تکمیلی</strong>				
		</div>
		<div>
			<p>توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است </p>
		</div>
	</div>
	






</div>









@endsection


@section('d_js')

<script type="text/javascript">
	$('.tabs_styled_2 ul li:nth-child(5) a').addClass('active');
</script>

@endsection