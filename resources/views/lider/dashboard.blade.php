
@extends('layouts.lider')


@section('d_css')


@endsection

@section('d_content')




<div class="tab-pane fade show active" >

	

	<div class="main_title_4">
		<h3><i class="icon_circle-slelected"></i> اطلاعات شخصی شما </h3>
	</div>
	
	
	<div class="indent_title_in">
		<i class="pe-7s-user"></i>
		<h3>اطلاعات شخصی  </h3>
		
	</div>
	<div class="wrapper_indent">
		<div class="row">
			<div class="col-md-6">
				<ul class="bullets">
					<li><strong>نام و نام خانوادگی :</strong> <small></small> </li>
					<li><strong>کد ملی :</strong> <small></small> </li>
					<li><strong>جنسیت :</strong> <small></small> </li>
					<li><strong>سال تولد :</strong> <small></small>  </li>
				</ul>
			</div>
			<div class="col-md-6">
				<ul class="bullets">
					<li><strong>نام پدر :</strong> <small></small> </li>
					<li><strong>شماره تماس مطب  :</strong> <small></small> </li>
					<li><strong>شماره همراه  :</strong> <small></small> </li>
					<li><strong>ایمیل :</strong> <small></small>  </li>
				</ul>
			</div>
			<div class="col-md-12">
				<ul class="bullets">
					<li><strong>کشور :</strong> <small></small> </li>
					<li><strong>شهر محل اقامت :</strong> <small></small> </li>
					<li><strong>زبان های تخصصی :</strong> <small></small> </li>
				</ul>
			</div>
			
		</div>
		<!-- /row-->
		<div class="col-md-12">
			<div class="margin-tb-20">
				<strong>توضیحات تکمیلی</strong>				
			</div>
			<div>
				<p>توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است توضیحات تکمیلی متن الکی است </p>
			</div>
		</div>
	</div>
	<!--  End wrapper indent -->
	

</div>




@endsection


@section('d_js')

<script type="text/javascript">
	$('.tabs_styled_2 ul li:nth-child(1) a').addClass('active');
</script>

@endsection