
@extends('layouts.lider')


@section('d_css')


<style type="text/css">
	article.blog figure img , article.blog figure {height: 250px !important}
	article.blog {
		border: 1px solid #ccc
	}
</style>

@endsection

@section('d_content')



<div class="tab-pane fade show active" >


	<div class="dis-block bg-red padding-top-20 white padding-bottom-15 margin-tb-20 text-center">
		<p>کاربر گرامی شما هیچ بیماری در لیست ندارید . </p>
	</div>

	<div class="main_title_4">
		<h3><i class="icon_circle-slelected"></i> شما 3 بیمار را رهبری میکنید </h3>
	</div>

	<article class="blog wow fadeIn">
		<div class="row no-gutters">
			<div class="col-md-5">
				<figure>
					<a href="">
						<img src="{{ asset('assets/img/doctor_1_carousel.jpg')}}" alt="">
						<div class="preview">
							
						</div>
					</a>
				</figure>
			</div>
			<div class="col-md-7">
				<div class="post_info">
					<small>20 / 10 / 96</small>
					<h3><a href="">نام بیمار  </a></h3>
					<p>ین یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد .</p>
					<ul>
						{{-- <li>
							<a href="" class="btn danger-light">ادامه مطلب</a>
						</li>
						<li class="pull-left"></li> --}}
					</ul>
				</div>
			</div>
		</div>
	</article>




</div>


@endsection


@section('d_js')

<script type="text/javascript">
	$('.tabs_styled_2 ul li:nth-child(2) a').addClass('active');
</script>

@endsection