
@extends('layouts.doctor')

@section('d_css')
<style type="text/css">
	.strip_list {border: 1px solid #e0e0e0}
	.strip_list h3 {
		overflow: hidden;
	    text-overflow: ellipsis;
	    white-space: nowrap;
	    width: 80%;
	}
</style>
@endsection

@section('d_content')



<div class="tab-pane fade show active" >
	
	<div class="bloglist singlepost">
		<p><img alt="" class="img-fluid" src="{{ asset('assets/img/blog-single.jpg')}}"></p>
		<h1>عنوان سوال عنوان سوال عنوان سوال عنوان سوال عنوان سوال عنوان سوال ؟</h1>
		<div class="postmeta">
			<ul>
				<li><i class="icon_folder-alt"></i> دسته بندی </li>
				<li><i class="icon_clock_alt"></i> 23/12/1396</li>
				<li><i class="icon_pencil-edit"></i> پزشک</li>
				<li><i class="icon-eye"></i> 20</li>
			</ul>
		</div>
		<!-- /post meta -->
		<div class="post-content">
			<div class="">
				<p>یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ </p>
			</div>

			<p>یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ یک متن الکی برای پاسخ </p>
		</div>
		<!-- /post -->
	</div>
	<hr>
	<div class="text-center">
		<a href="{{ url('/doctor/FAQ') }}" class="btn btn-base info-dark">سوالات دیگر</a>
	</div>

</div>




@endsection


@section('d_js')

<script type="text/javascript">
	$('.tabs_styled_2 ul li:nth-child(3) a').addClass('active');
</script>

@endsection