
@extends('layouts.doctor')

@section('d_css')
<style type="text/css">
	.strip_list {border: 1px solid #e0e0e0}
	.strip_list h3 {
		overflow: hidden;
	    text-overflow: ellipsis;
	    white-space: nowrap;
	    width: 80%;
	}
</style>
@endsection

@section('d_content')



<div class="tab-pane fade show active" >
	<div class="main_title_4">
		<h3><i class="icon_circle-slelected"></i> این پزشک 10 مقاله ی دیگر هم منتشر کرده است <a href="{{ url('/doctor/articles') }}" class="pull-left white">دیدن همه <i class="pe-7s-angle-left"></i></a></h3>
	</div>
	<div class="bloglist singlepost">

		<div class="row">
			<strong class="margin-bottom-30 font-md"> صفحه ی مشاهده ی جزییات مقاله ی <span class="teal">عنوان مقاله عنوان مقاله عنوان مقاله عنوان مقاله عنوان مقاله عنوان مقاله</span></strong>
		</div>
		<p><img alt="" class="img-fluid" src="{{ asset('assets/img/blog-single.jpg')}}"></p>
		<h1>عنوان مقاله عنوان مقاله عنوان مقاله عنوان مقاله عنوان مقاله عنوان مقاله ؟</h1>
		<div class="postmeta">
			<ul>
				<li><i class="icon_folder-alt"></i> دسته بندی </li>
				<li><i class="icon_clock_alt"></i> 23/12/1396</li>
				<li><i class="icon_pencil-edit"></i> پزشک</li>
				<li><i class="icon-eye"></i> 20</li>
			</ul>
		</div>
		<!-- /post meta -->
		<div class="post-content">
			<div class="">
				<p>یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله </p>
			</div>

			<p>یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله یک متن الکی برای مقاله </p>
		</div>
		<!-- /post -->
	</div>
	<hr>
	<div class="text-center">
		<a href="{{ url('/doctor/articles') }}" class="btn  success-dark">مشاهده همه مقالات این پزشک</a>
	</div>

</div>




@endsection


@section('d_js')

<script type="text/javascript">
	$('.tabs_styled_2 ul li:nth-child(2) a').addClass('active');
</script>

@endsection