
@extends('layouts.doctor')


@section('d_css')


@endsection

@section('d_content')




<div class="tab-pane fade show active" >
	<div class="main_title_4">
		<h3><i class="icon_circle-slelected"></i>مشخصات و اطلاعاتی درباره این پزشک را بخوانید</h3>
	</div>
	<div class="indent_title_in">
		<i class="pe-7s-user"></i>
		<h3>سوابق و مدارک حرفه ای حرفه ای</h3>
		<p>این یک  متن الکی است و هیچ کاربردی ندارد .</p>
	</div>
	<div class="wrapper_indent">
		<p>این یک  متن الکی است و هیچ کاربردی ندارد  این یک  متن الکی است و هیچ کاربردی ندارد  این یک  متن الکی است و هیچ کاربردی ندارد  این یک  متن الکی است و هیچ کاربردی ندارد این یک  متن الکی است و هیچ کاربردی ندارد این یک  متن الکی است و هیچ کاربردی ندارد </p>
		<h6>تخصص ها</h6>
		<div class="row">
			<div class="col-lg-6">
				<ul class="bullets">
					<li>تخصص ها 1 </li>
					<li>تخصص ها 2 </li>
					<li>تخصص ها 3 </li>
					<li>تخصص ها 4  </li>
				</ul>
			</div>
			<div class="col-lg-6">
				<ul class="bullets">
					<li>تخصص ها 1 </li>
					<li>تخصص ها 2 </li>
					<li>تخصص ها 3 </li>
					<li>تخصص ها 4  </li>
				</ul>
			</div>
		</div>
		<!-- /row-->
	</div>
	<!-- /wrapper indent -->
	
	<hr>
	
	<div class="indent_title_in">
		<i class="pe-7s-news-paper"></i>
		<h3>تحصیلات </h3>
		<p>این یک  متن الکی است و هیچ کاربردی ندارد .</p>
	</div>
	<div class="wrapper_indent">
		<p>این یک  متن الکی است و هیچ کاربردی ندارد .این یک  متن الکی است و هیچ کاربردی ندارد .این یک  متن الکی است و هیچ کاربردی ندارد .این یک  متن الکی است و هیچ کاربردی ندارد .</p>
		<h6>مدارک تحصیلی</h6>
		<ul class="list_edu">
			<li><strong>دانشگاه پزشکی تهران </strong> - تخصص دندانپزشکی</li>
			<li><strong>مرکز پزشکی مونتفیور</strong> -  پزشکی داخلی</li>
			<li><strong>دانشگاه پزشکی تهران</strong> - تخصص قلب و عروق</li>
		</ul>
	</div>
	<!--  End wrapper indent -->

	<hr>
	
	<div class="indent_title_in">
		<i class="pe-7s-map-marker"></i>
		<h3>آدرس محل کار </h3>
		<p>این یک  متن الکی است و هیچ کاربردی ندارد .</p>
	</div>
	<div class="wrapper_indent">
		<p>این یک  متن الکی است و هیچ کاربردی ندارد .این یک  متن الکی است و هیچ کاربردی ندارد .این یک  متن الکی است و هیچ کاربردی ندارد .این یک  متن الکی است و هیچ کاربردی ندارد .</p>
		
	</div>
	<!--  End wrapper indent -->
	

</div>




@endsection


@section('d_js')

<script type="text/javascript">
	$('.tabs_styled_2 ul li:nth-child(1) a').addClass('active');
</script>

@endsection