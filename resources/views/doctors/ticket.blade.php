@extends('layouts.doctor')

@section('d_css')

<link href="{{ asset('assets/css/vendor/bootstrap-datepicker.css')}}" rel="stylesheet">
<style type="text/css">
	article.blog figure img , article.blog figure {height: 250px !important}
</style>
@endsection

@section('d_content')

	

	<div class="tab-pane fade show active" >
		<div class="box_general_2 add_bottom_45">

			
			<div class="main_title_4">
				<h3><i class="icon_circle-slelected"></i>تاریخ و ساعت مراجعه ی خود را انتخاب کنید </h3>
			</div>
			
			<div class="row add_bottom_45">
				<div class="col-lg-7">
					<div class="form-group">
						<div class="control-group">
			                <label class="control-label" for="datepicker1">برای انتخاب تاریخ کلیک کنید :</label>
			                <div class="controls">
			                    <div class="input-append">
			                        <input id="datepicker1" class="input-small" type="text" placeholder="یک تاریخ انتخاب کنید ">
			                        <button id="datepicker1btn" class="btn" type="button"><i class="icon-calendar"></i></button>
			                    </div>
			                </div>
			            </div>
						{{-- <input type="hidden" id="my_hidden_input">
						<ul class="legend">
							<li><strong></strong>هستم</li>
							<li><strong></strong>نیستم</li>
						</ul> --}}
					</div>
				</div>
				<div class="col-lg-5">
					<h4>ساعت ویزیت را انتخاب کنید </h4>
					<ul class="time_select version_2 add_top_20">
						<li>
							<input type="radio" id="radio1" name="radio_time" value="09.30am">
							<label for="radio1">09.30 صبح</label>
						</li>
						<li>
							<input type="radio" id="radio2" name="radio_time" value="10.00am">
							<label for="radio2">10.00 صبح</label>
						</li>
						<li>
							<input type="radio" id="radio3" name="radio_time" value="10.30am">
							<label for="radio3">10.30 صبح</label>
						</li>
						<li>
							<input type="radio" id="radio4" name="radio_time" value="11.00am">
							<label for="radio4">11.00 صبح</label>
						</li>
						<li>
							<input type="radio" id="radio5" name="radio_time" value="11.30am">
							<label for="radio5">11.30 صبح</label>
						</li>
						<li>
							<input type="radio" id="radio6" name="radio_time" value="12.00am">
							<label for="radio6">12.00 صبح</label>
						</li>
						<li>
							<input type="radio" id="radio7" name="radio_time" value="01.30pm">
							<label for="radio7">01.30 عصر</label>
						</li>
						<li>
							<input type="radio" id="radio8" name="radio_time" value="02.00pm">
							<label for="radio8">02.00 عصر</label>
						</li>
						<li>
							<input type="radio" id="radio9" name="radio_time" value="02.30pm">
							<label for="radio9">02.30 عصر</label>
						</li>
						<li>
							<input type="radio" id="radio10" name="radio_time" value="03.00pm">
							<label for="radio10">03.00 عصر</label>
						</li>
						<li>
							<input type="radio" id="radio11" name="radio_time" value="03.30pm">
							<label for="radio11">03.30 عصر</label>
						</li>
						<li>
							<input type="radio" id="radio12" name="radio_time" value="04.00pm">
							<label for="radio12">04.00 عصر</label>
						</li>
					</ul>
				</div>
			
			<!-- /row -->
			
			</div>
			
			<div class="">
				<div class="box_general_3 booking">
					<div class="title">
						<h3>اطلاعات خود را تکمیل نمایید </h3>
					</div>
					
						<div class="row">
							<div class="col-md-6 ">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="نام " name="name_booking" id="name_booking">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="نام خانوادگی" name="lastname_booking" id="lastname_booking">
								</div>
							</div>
						</div>
						<!-- /row -->
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="number" class="form-control" placeholder="شماره تماس " name="email_booking" id="email_booking">
								</div>
							</div>
						</div>
						<!-- /row -->
						
						
						<!-- /row -->
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<textarea rows="5" id="booking_message" name="booking_message" class="form-control" style="height:80px;" placeholder="بیماری یا مشکل خود را شرح دهید "></textarea>
								</div>
							</div>
						</div>
						<!-- /row -->
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" id="verify_booking" class="form-control" placeholder="کد امنیتی : 3 + 1 =?">
								</div>
							</div>
						</div>
						<!-- /row -->
						<hr>
						<div style="position:relative;"><input type="submit" class="btn_1 full-width" value="ثبت نوبت" id="submit-booking"></div>
					
				</div>
			</div>
			
		</div>
	</div>
	<!-- /tab_1 -->
				
				
				

				
			

@endsection


@section('d_js')

 <script src="{{ asset('assets/js/bootstrap-datepicker.min.js')}}"></script>
 <script src="{{ asset('assets/js/bootstrap-datepicker.fa.js')}}"></script>
    <script>
			$(document).ready(function() {
                

                $("#datepicker1btn").click(function(event) {
                    event.preventDefault();
                    $("#datepicker1").focus();
                })
            
                $("#datepicker1").datepicker({
                    showOtherMonths: true,
                    isRTL: true,
                    dateFormat: "yy/m/d"
                });
            
               $('.tabs_styled_2 ul li:nth-child(4) a').addClass('active');
            });
	</script>

@endsection