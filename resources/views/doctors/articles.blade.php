

@extends('layouts.doctor')

@section('d_css')

<style type="text/css">
	article.blog figure img , article.blog figure {height: 250px !important}
</style>
@endsection

@section('d_content')




<!-- /tab_2 -->

<div class="tab-pane fade show active" >
	<div class="main_title_4">
		<h3><i class="icon_circle-slelected"></i> مقالات منتشر شده توسط این پزشک را بخوانید</h3>
	</div>
	<div class="reviews-container">
		<div class="row">
			

			<article class="blog wow fadeIn">
				<div class="row no-gutters">
					<div class="col-md-5">
						<figure>
							<a href="{{ url('doctor/articles/slug') }}">
								<img src="{{ asset('assets/img/blog-1.jpg')}}" alt="">
								<div class="preview">
									<span>ادامه مطلب </span>
								</div>
							</a>
						</figure>
					</div>
					<div class="col-md-7">
						<div class="post_info">
							<small>20 / 10 / 96</small>
							<h3><a href="">عنوان مقاله عنوان مقاله </a></h3>
							<p>ین یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد .</p>
							<ul>
								<li>
									<a href="{{ url('doctor/articles/slug') }}" class="btn danger-light">ادامه مطلب</a>
								</li>
								<li class="pull-left"><i class="icon_comment_alt"></i> 20</li>
							</ul>
						</div>
					</div>
				</div>
			</article>

			<article class="blog wow fadeIn">
				<div class="row no-gutters">
					<div class="col-md-5">
						<figure>
							<a href="{{ url('doctor/articles/slug') }}">
								<img src="{{ asset('assets/img/blog-1.jpg')}}" alt="">
								<div class="preview">
									<span>ادامه مطلب </span>
								</div>
							</a>
						</figure>
					</div>
					<div class="col-md-7">
						<div class="post_info">
							<small>20 / 10 / 96</small>
							<h3><a href="">عنوان مقاله عنوان مقاله </a></h3>
							<p>ین یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد .</p>
							<ul>
								<li>
									<a href="{{ url('doctor/articles/slug') }}" class="btn danger-light">ادامه مطلب</a>
								</li>
								<li class="pull-left"><i class="icon_comment_alt"></i> 20</li>
							</ul>
						</div>
					</div>
				</div>
			</article>

			<article class="blog wow fadeIn">
				<div class="row no-gutters">
					<div class="col-md-5">
						<figure>
							<a href="{{ url('doctor/articles/slug') }}">
								<img src="{{ asset('assets/img/blog-1.jpg')}}" alt="">
								<div class="preview">
									<span>ادامه مطلب </span>
								</div>
							</a>
						</figure>
					</div>
					<div class="col-md-7">
						<div class="post_info">
							<small>20 / 10 / 96</small>
							<h3><a href="">عنوان مقاله عنوان مقاله </a></h3>
							<p>ین یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد .</p>
							<ul>
								<li>
									<a href="{{ url('doctor/articles/slug') }}" class="btn danger-light">ادامه مطلب</a>
								</li>
								<li class="pull-left"><i class="icon_comment_alt"></i> 20</li>
							</ul>
						</div>
					</div>
				</div>
			</article>

			<article class="blog wow fadeIn">
				<div class="row no-gutters">
					<div class="col-md-5">
						<figure>
							<a href="{{ url('doctor/articles/slug') }}">
								<img src="{{ asset('assets/img/blog-1.jpg')}}" alt="">
								<div class="preview">
									<span>ادامه مطلب </span>
								</div>
							</a>
						</figure>
					</div>
					<div class="col-md-7">
						<div class="post_info">
							<small>20 / 10 / 96</small>
							<h3><a href="">عنوان مقاله عنوان مقاله </a></h3>
							<p>ین یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد این یک متن الکی است و هیچ کاربردی ندارد .</p>
							<ul>
								<li>
									<a href="{{ url('doctor/articles/slug') }}" class="btn danger-light">ادامه مطلب</a>
								</li>
								<li class="pull-left"><i class="icon_comment_alt"></i> 20</li>
							</ul>
						</div>
					</div>
				</div>
			</article>

		</div>
		<!-- /row -->
	</div>
	<!-- End review-container -->
</div>




@endsection


@section('d_js')
<script type="text/javascript">
	$('.tabs_styled_2 ul li:nth-child(2) a').addClass('active');
</script>

@endsection