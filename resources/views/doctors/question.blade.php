@extends('layouts.doctor')

@section('d_css')

@endsection

@section('d_content')

	

	<div class="tab-pane fade show active" >
		<div class="box_general_2 add_bottom_45">

			<div class="dis-block bg-red padding-top-20 white padding-bottom-15 margin-tb-20 text-center">
				<p>برای این بخش ابتدا باید وارد سایت شوید </p>
				<a href="" class="btn btn-base success-light">ورود به سایت</a>
			</div>

			{{-- <div class="main_title_4">
				<h3><i class="icon_circle-slelected"></i>میتوانید سوال ، انتقاد و پیشنهادات خود را مطرح نمایید </h3>
			</div> --}}
			
			<div class="row add_bottom_45">
				
				<div class="col-lg-12">
					<div class="box_general_3 booking">
						<div class="title">
							<h3>میتوانید سوال ، انتقاد و پیشنهادات خود را مطرح نمایید </h3>
						</div>
						
							<strong class="red">توجه : درصورت بی احترامی و استفاده از کلمات رکیک ، اقدام جدی را به دنبال دارد و در صورت نیاز اکانت کاربر غیر فعال خواهد شد .</strong>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<input type="text" id="verify_booking" class="form-control" placeholder="دسته ی موضوعی">
									</div>
								</div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group">
										<textarea rows="5" id="booking_message" name="booking_message" class="form-control" style="height:100px;" placeholder="سوال ، انتقاد و پیشنهاد خود را وارد  کنید تا پزشک به آن پاسخ دهد "></textarea>
									</div>
								</div>
							</div>
							<!-- /row -->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<input type="text" id="verify_booking" class="form-control" placeholder="کد امنیتی : 3 + 1 =?">
									</div>
								</div>
							</div>
							<!-- /row -->
							<hr>
							<div style="position:relative;"><input type="submit" class="btn_1 full-width" value="ثبت سوال" id="submit-booking"></div>
						
					</div>
				</div>

			</div>
			<!-- /row -->

			<div class="row justify-content-center">
				<div class="col-lg-8">
					<div id="confirm">
						<div class="icon icon--order-success svg add_bottom_15">
							<svg xmlns="http://www.w3.org/2000/svg" width="72" height="72">
								<g fill="none" stroke="#8EC343" stroke-width="2">
									<circle cx="36" cy="36" r="35" style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
									<path d="M17.417,37.778l9.93,9.909l25.444-25.393" style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
								</g>
							</svg>
						</div>
					<h2>پیام شما با موفقیت ثبت شد .!</h2>
					<p>لطفا به بخش پنل کاربری خود و قسمت سوالات من مراجعه نمایید  . </p>
					</div>
				</div>
			</div>
			<!-- /row -->
			
			
		</div>
	</div>
	<!-- /tab_1 -->
				
				
				

				
			

@endsection


@section('d_js')

   <script>
			$(document).ready(function() {
                

               $('.tabs_styled_2 ul li:nth-child(5) a').addClass('active');
            });
	</script>

@endsection