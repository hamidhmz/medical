

@extends('layouts.doctor_dashboard')

@section('d_css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendor/sweetalert2.css') }}">
<style type="text/css">
	
	.strip_list {
		border: 1px solid #ccc;
	}

</style>

@endsection

@section('d_content')




<!-- /tab_2 -->

<div class="tab-pane fade show active" >



	<div class="tabs_styled_3">
		<ul class="nav nav-tabs" role="tablist">
			
			<li class="">
				<a href="{{ url('/specialist/questions') }}" class="nav-link active" >سوالات پاسخ داده نشده</a>
			</li>
			<li class="">
				<a href="{{ url('/specialist/questions/answered') }}" class="nav-link " >سوالات پاسخ داده شده </a>
			</li>
			
		</ul>
		<!--/nav-tabs -->

		<div class="tab-content">
			<div class="main_title_4">
				<h3><i class="icon_circle-slelected"></i> شما 13 سوال جدید پاسخ داده نشده دارید </h3>
			</div>

			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام کاربر</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li><a href="#0"  class="btn_listing">رد کردن</a></li>
					<li><a href="#!" onclick="answer('متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی ' , 1)">پاسخ به سوال</a></li>
				</ul>
			</div>

			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام کاربر</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li><a href="#0"  class="btn_listing">رد کردن</a></li>
					<li><a href="#!" onclick="answer('متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی ' , 1)">پاسخ به سوال</a></li>
				</ul>
			</div>
			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام کاربر</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li><a href="#0"  class="btn_listing">رد کردن</a></li>
					<li><a href="#!" onclick="answer('متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی ' , 1)">پاسخ به سوال</a></li>
				</ul>
			</div>
			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام کاربر</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li><a href="#0"  class="btn_listing">رد کردن</a></li>
					<li><a href="#!" onclick="answer('متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی ' , 1)">پاسخ به سوال</a></li>
				</ul>
			</div>
			<div class="strip_list wow fadeIn">
				<figure>
					<a href=""><img src="{{ asset('assets/img/doctor_listing_1.jpg')}}" alt=""></a>
				</figure>
				<small>دسته ی موضوعی</small>
				<h3 class="margin-tb-5">نام کاربر</h3>
				<p>متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی </p>
				
				<ul>
					<li><a href="#0"  class="btn_listing">رد کردن</a></li>
					<li><a href="#!" onclick="answer('متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی متن سوال کاربر یک متن الکی ' , 1)">پاسخ به سوال</a></li>
				</ul>
			</div>
			
			<!-- End review-container -->
		</div>
	</div>
</div>


@endsection


@section('d_js')

<script type="text/javascript" src="{{ asset('assets/js/sweetalert2.js') }}"></script>

<script type="text/javascript">
	$('.tabs_styled_2 ul li.nav-item:nth-child(5) a').addClass('active');

	function answer(argument , int) {
		// body...

		swal({
		  html : 
		  '<div class="">' +
				'<div class=" row">' +
					'<strong class="margin-tb-5 teal" style="font-size: 21px">متن سوال </strong>' +
					'<p class="text-right padding-5" style="font-size: 12px">'+argument+'</p>' +
				'</div>' +
				'<hr>' +
				'<div class=" row">' +
					'<strong class="margin-tb-5 teal" style="font-size: 21px">متن پاسخ خود را وارد کنید  </strong>' +
					'<div class="col-lg-12">' +
						'<div class="form-group">' +
							'<textarea rows="5" id="booking_message" name="booking_message" class="form-control" style="height:180px;" placeholder="متن پاسخ به سوال کاربر "></textarea>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>' ,

		  
		  // input: 'email',
		  showCancelButton: true,
		  confirmButtonText: 'ارسال پاسخ',
		  showLoaderOnConfirm: true,
		  
		  preConfirm: function (email) {
		    return new Promise(function (resolve, reject) {
		      
		      setTimeout(function() {
		        if (email === 'taken@example.com') {
		          reject('This email is already taken.')
		        } else {
		          resolve()
		        }
		      }, 2000)
		    })
		  },
		  allowOutsideClick: false
		}).then(function (email) {
		  swal({
		    type: 'success',
		    title: 'تبریک ',
		    html: 'پاسخ شما با موفقیت ارسال شد'
		  })
		})

	// 	preConfirm: function () {
	//     return new Promise(function (resolve, reject) {
	        
	//         $.ajax({
	//             url: '/User/BadVote',
	//             type: 'POST',
	//             datatype: 'json',
	//             data: { id:tid }


	//         }).done(function (data) {
	//             if (data) {
	//                 resolve()
	                
	//             }
	           
	//         })
	     
	//     })
	//   },
	//   allowOutsideClick: false
	// }).then(function (argument) {
	//   swal({
	//     type: 'success',
	//     text: 'با موفقیت ثبت شد توسط تیم پشتیبانی بررسی خواهد شد '
	//   })
	// })

	}


</script>

@endsection