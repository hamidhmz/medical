

@extends('layouts.doctor_dashboard')

@section('d_css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendor/bootstrap-fileinput.css') }}">

<style type="text/css">
	article.blog figure img , article.blog figure {height: 250px !important}
</style>
@endsection

@section('d_content')




<!-- /tab_2 -->

<div class="tab-pane fade show active" >



	<div class="tabs_styled_3">
		<ul class="nav nav-tabs" role="tablist">
			
			<li class="">
				<a href="{{ url('/specialist/FAQ') }}" class="nav-link" >سوالات منتشر شده</a>
			</li>
			<li class="">
				<a href="{{ url('/specialist/FAQ/new') }}" class="nav-link active" >ایجاد سوال جدید </a>
			</li>
			
		</ul>
		<!--/nav-tabs -->

		<div class="tab-content">



			{{-- <div class="main_title_4">
				<h3><i class="icon_circle-slelected"></i> شما 13 مقاله منتشر کرده اید </h3>
			</div> --}}
			<form class="box_general_3 booking">
				<div class="title">
					<h3>انتشار سوال و جواب </h3>
				</div>
				
					<div class="row">
						<div class="col-md-6 ">
							<div class="form-group">
								<label>سوال</label>
								<input type="text" class="form-control" placeholder="سوال را وارد کنید" name="name_booking" id="name_booking">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>دسته ی موضوعی</label>
								<input type="text" class="form-control" placeholder="دسته ی موضوعی را وارد کنید" name="lastname_booking" id="lastname_booking">
							</div>
						</div>
					</div>
					
					
					
					<!-- /row -->
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<textarea rows="5" id="booking_message" name="booking_message" class="form-control" style="height:180px;" placeholder="متن جواب به سوال را وارد کنید "></textarea>
							</div>
						</div>
					</div>
					<!-- /row -->
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                    </div>

                                    <div>
                                        <div class="btn carrot  btn-file">
                                            <span class="fileinput-new"> انتخاب تصویر ( 800x530 ) </span>
                                            <span class="fileinput-exists"> تغییر </span>
                                            <input type="file" name="..."> 
                                        </div>
                                        <a href="javascript:;" class="btn default  fileinput-exists" data-dismiss="fileinput"> حذف </a>
                                    </div>
                                </div>
                               
                            </div>
                            <div class="margin-top-10">
                                <a href="" class="btn  btn-md danger-light pull-right"> انصراف </a>
                            </div>
						</div>
					</div>
					<!-- /row -->
					<hr>
					<div style="position:relative;"><input type="submit" class="btn_1 full-width" value="انتشار سوال و جواب " id="submit-booking"></div>
				
			</form>
			<!-- End review-container -->
		</div>
	</div>
</div>




@endsection


@section('d_js')
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-fileinput.js') }}"></script>

<script type="text/javascript">
	$('.tabs_styled_2 ul li.nav-item:nth-child(3) a').addClass('active');
</script>

@endsection