@extends('layouts.doctor_dashboard')

@section('d_css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendor/bootstrap-fileinput.css') }}">

<style type="text/css">
	.items {
		margin: 10px 0;
		position: relative;
	}
	.remove-me {
		position: absolute;
		top: 0;
		left: 0
	}
	.form_title h3 strong {
		    background-color: #A52004;
	}
</style>
@endsection

@section('d_content')

	

	<div class="tab-pane fade show active" >
		<div class="box_general_2 add_bottom_45">

			
			
			<div class="row add_bottom_45">
				
				<div class="col-lg-12">
					<div class="box_general_3 booking doctor-setting">
						<div class="title">
							<h3>تکمیل اطلاعات </h3>
						</div>
						
							<strong class="red" >توجه : لطفا تمامی موارد * دار را تکمیل نمایید  .</strong>
							<div class="clearfix margin-bottom-20"></div>

							<div class="form_title">
								<h3><strong>1</strong>اطلاعات هویتی </h3>
								
							</div>
							<form class="step">
								<div class="row">
									<div class="col-md-6 ">
										<div class="form-group">
											<label>نام  <span>*</span></label>
											<input type="text" class="form-control" placeholder="نام خود را وارد کنید" value="از دیتابیس میشینه" name="name_booking" id="name_booking">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>نام خانوادگی <span>*</span></label>
											<input type="text" class="form-control" placeholder="نام خانوادگی خود را وارد کنید" value="از دیتابیس میشینه" name="lastname_booking" id="lastname_booking">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 ">
										<div class="form-group">
											<label>جنسیت  <span>*</span></label><br>
											<select>
												<option selected="selected" disabled="disabled">جنسیت خود را انتخاب کنید</option>
												<option>مرد</option>
												<option>زن</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>سال تولد  <span>*</span></label>
											<input type="text" class="form-control"  value="از دیتابیس میشینه" name="lastname_booking" id="lastname_booking">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 ">
										<div class="form-group">
											<label>کد ملی  <span>*</span></label>
											<input type="text" class="form-control"  value="از دیتابیس میشینه" name="lastname_booking" id="lastname_booking">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>شماره ی نظام پزشکی  <span>*</span></label>
											<input type="text" class="form-control"  value="از دیتابیس میشینه" name="lastname_booking" id="lastname_booking">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 ">
										<div class="form-group">
											<label>ایمیل  <span>*</span></label>
											<input type="text" class="form-control"  value="از دیتابیس میشینه" name="lastname_booking" id="lastname_booking">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>شماره ی تماس ثابت  <span>*</span></label>
											<input type="text" class="form-control"  value="از دیتابیس میشینه" name="lastname_booking" id="lastname_booking">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 ">
										<div class="form-group">
											<label>شماره ی تماس همراه  <span>*</span></label>
											<input type="text" class="form-control"  value="از دیتابیس میشینه" name="lastname_booking" id="lastname_booking">
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label> استان محل سکونت  <span>*</span></label>
											<input type="text" class="form-control"  value="از دیتابیس میشینه" name="lastname_booking" id="lastname_booking">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 ">
										<div class="form-group">
											<label>آدرس مطب  <span>*</span></label>
											<textarea rows="5" id="booking_message" name="booking_message" class="form-control" style="height:80px;" placeholder="آدرس مطب ار دیتابیس "></textarea>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>نام شهر محل سکونت  <span>*</span></label>
											<input type="text" class="form-control"  value="از دیتابیس میشینه" name="lastname_booking" id="lastname_booking">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div style="position:relative;"><input type="submit" class="btn_1 btn-base" value="ذخیره تغییرات" id="submit-booking"></div>
									</div>
								</div>
							</form>
							<hr>
							<div class="clearfix margin-bottom-20"></div>

							<div class="form_title">
								<h3><strong>2</strong>اطلاعات تحصیلی </h3>
								
							</div>
							<form class="step">

								<div class="row">
									<div class="col-md-12 ">
										<div class="form-group">
											<strong class="red">توجه : لطفا به تعداد مدارک تحصیلی خود ، فیلد ایجاد نمایید </strong><br><br>
											<label>مدارک تحصیلی <span>*</span></label>
											<div id="field">
												<div class="items">
													<input autocomplete="off" class="input form-control" style="" id="field1" name="prof1" type="text" placeholder="مدرک تحصیلی " data-items="8"/>
												</div>
											</div>
											<button id="b1" class="btn add-more" type="button">+ افزودن فیلد</button>
                
											
										</div>
									</div>
									
								</div>

								<hr style="width: 60%">
								<!-- /row -->
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<textarea rows="5" id="booking_message" name="booking_message" class="form-control" style="height:100px;" placeholder="توضیح مختصری درباره مدارک و تحصیلات  "></textarea>
										</div>
									</div>
								</div>
								<!-- /row -->
								<div class="row">
									<div class="col-md-12">
										<div style="position:relative;"><input type="submit" class="btn_1 btn-base" value="ذخیره تغییرات" id="submit-booking"></div>
									</div>
								</div>
							</form>
							<!-- /row -->
							<hr>
							<div class="clearfix margin-bottom-20"></div>

							<div class="form_title">
								<h3><strong>3</strong>رزومه و تخصص </h3>
								
							</div>
							<form class="step">
								
								<div class="row">
									<div class="col-md-12 ">
										<div class="form-group">
											<strong class="red">توجه : لطفا به تعداد تخصص های خود ، فیلد ایجاد نمایید </strong><br><br>
											<label>تخصص ها <span>*</span></label>
											<div id="fielddd">
												<div class="items">
													<input autocomplete="off" class="input form-control" style="" id="fielddd1" name="prof1" type="text" placeholder="تخصص خود را وارد کنید " data-items="8"/>
												</div>
											</div>
											<button id="b12" class="btn add-more2" type="button">+ افزودن فیلد</button>
                
											
										</div>
									</div>
									
								</div>

								<div class="row">
									<div class="col-md-12 ">
										<div class="form-group">
											<label>فایل رزومه خود را آپلود کنید  <span>*</span></label> <br><br>
											<input type="file" name="">
										</div>
									</div>
									
								</div>
								<div class="row">
									<div class="col-md-12 ">
										<div class="form-group">
											<label>توضیح مختصری درباره تخصص های خود <span>*</span></label>
											<textarea rows="5" id="booking_message" name="booking_message" class="form-control" style="height:100px;" placeholder="توضیح مختصری درباره تخصص های خود"></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div style="position:relative;"><input type="submit" class="btn_1 btn-base" value="ذخیره تغییرات" id="submit-booking"></div>
									</div>
								</div>
							</form>

							<hr>
							<div class="clearfix margin-bottom-20"></div>

							<div class="form_title">
								<h3><strong>4</strong>عکس پروفایل </h3>
								
							</div>
							<form class="step">
								
								<div class="row">
									<div class="col-md-12 ">

										<div class="form-group">
			                                <div class="fileinput fileinput-new" data-provides="fileinput">
			                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
			                                        
			                                    </div>
			                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
			                                    </div>

			                                    <div>
			                                        <div class="btn carrot  btn-file">
			                                            <span class="fileinput-new"> انتخاب تصویر پروفایل ( 550x550 ) </span>
			                                            <span class="fileinput-exists"> تغییر </span>
			                                            <input type="file" name="..."> 
			                                        </div>
			                                        <a href="javascript:;" class="btn default  fileinput-exists" data-dismiss="fileinput"> حذف </a>
			                                    </div>
			                                </div>
			                               
			                            </div>
			                            <div class="margin-top-10">
			                                <a href="" class="btn btn-base success-light pull-right"> آپلود </a>
			                                <a href="" class="btn  btn-md danger-light pull-right"> انصراف </a>
			                            </div>

									</div>
								</div>
							</form>
					</div>
				</div>

			</div>
			<!-- /row -->

			
			
			
		</div>
	</div>
	<!-- /tab_1 -->
				
				
				

				
			

@endsection


@section('d_js')
	
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-fileinput.js') }}"></script>
   <script>
			$(document).ready(function() {
               $('.tabs_styled_2 ul li:nth-child(1) a').addClass('active');
                

            });

			$(document).ready(function(){
			    var next = 1;
			    $(".add-more").click(function(e){
			        e.preventDefault();
			        var addto = "#field" + next;
			        var addRemove = "#field" + (next);
			        next = next + 1;
			        var newIn = '<div class="items"><input autocomplete="off" placeholder="مدرک تحصیلی ' + next  + '" class="input form-control" id="field' + next + '" name="field' + next + '" type="text"></div>';
			        var newInput = $(newIn);
			        var removeBtn = '<button id="remove' + (next - 1) + '" class="btn btn-danger remove-me" >-</button></div><div id="field">';
			        var removeButton = $(removeBtn);
			        $(addto).after(newInput);
			        $(addRemove).after(removeButton);
			        $("#field" + next).attr('data-source',$(addto).attr('data-source'));
			        $("#count").val(next);  
			        
			            $('.remove-me').click(function(e){
			                e.preventDefault();
			                var fieldNum = this.id.charAt(this.id.length-1);
			                var fieldID = "#field" + fieldNum;
			                $(this).remove();
			                $(fieldID).remove();
			            });
			    });
			    

			    
			});




	</script>
	<script type="text/javascript">
		
			$(document).ready(function(){
			    var next = 1;
			    $(".add-more2").click(function(e){

			        e.preventDefault();
			        var addto = "#fielddd" + next;
			        var addRemove = "#fielddd" + (next);
			        next = next + 1;
			        
			        var newIn = '<div class="items"><input autocomplete="off" placeholder="تخصص ' + next + ' خود را وارد کنید  " class="input form-control" id="fielddd' + next + '" name="field' + next + '" type="text"></div>';
			        var newInput = $(newIn);
			        var removeBtn = '<button id="remov' + (next - 1) + '" class="btn btn-danger remove-me" >-</button></div><div id="fielddd">';
			        var removeButton = $(removeBtn);
			        $(addto).after(newInput);
			        $(addRemove).after(removeButton);
			        $("#fielddd" + next).attr('data-source',$(addto).attr('data-source'));
			        $("#countd").val(next);  
			        
			            $('.remove-me').click(function(e){
			                e.preventDefault();
			                var fieldNum = this.id.charAt(this.id.length-1);
			                var fieldID = "#fielddd" + fieldNum;
			                $(this).remove();
			                $(fieldID).remove();
			            });
			    });
			    

			    
			});
	</script>

@endsection