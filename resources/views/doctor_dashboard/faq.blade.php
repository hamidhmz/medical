
@extends('layouts.doctor_dashboard')

@section('d_css')
<style type="text/css">
	.strip_list {border: 1px solid #e0e0e0}
	.strip_list h3 {
		overflow: hidden;
	    text-overflow: ellipsis;
	    white-space: nowrap;
	    width: 80%;
	}
</style>
@endsection

@section('d_content')



<div class="tab-pane fade show active" >


	<div class="tabs_styled_3">
		<ul class="nav nav-tabs" role="tablist">
			
			<li class="">
				<a href="{{ url('/specialist/FAQ') }}" class="nav-link active" >سوالات منتشر شده</a>
			</li>
			<li class="">
				<a href="{{ url('/specialist/FAQ/new') }}" class="nav-link" >ایجاد سوال جدید </a>
			</li>
			
		</ul>
		<!--/nav-tabs -->

		<div class="tab-content">



			<div class="main_title_4">
				<h3><i class="icon_circle-slelected"></i> شما 14 سوال و جواب را انتشار داده اید  </h3>
			</div>
			<div class="reviews-container">
				<div class="row">
					
					<div class="col-xs-12 col-lg-12">

						<div class="strip_list wow fadeIn">
							<a href="#0" class="wish_bt" data-toggle="tooltip" title="افزودن به علاقه ها"></a>
							<figure>
								<a href=""><img src="{{ asset('assets/img/blog-5.jpg')}}" alt=""></a>
							</figure>
							<h3 class="margin-tb-5">عنوان سوال عنوان سوال عنوان سوال عنوان سوال عنوان سوال عنوان سوال </h3>
							<p>یکی دو خط توضیح  یکی دو خط توضیح   یکی دو خط توضیح   یکی دو خط توضیح یکی دو خط توضیح </p>
							<span class="rating">
								<i class="icon-eye" style="color: #000"></i>
								<small>(145)</small>
							</span>
							<span class="rating">
								<i class="icon-calendar" style="color: #000"></i>
								<small>10 / 10 / 96</small>
							</span>
							<ul>
								<li></li>
								<li><a href="{{ url('/doctor/FAQ/slug') }}">خواندن بیشتر </a></li>
							</ul>
						</div>

						<div class="strip_list wow fadeIn">
							<a href="#0" class="wish_bt" data-toggle="tooltip" title="افزودن به علاقه ها"></a>
							<figure>
								<a href=""><img src="{{ asset('assets/img/blog-5.jpg')}}" alt=""></a>
							</figure>
							<h3 class="margin-tb-5">عنوان سوال عنوان سوال عنوان سوال عنوان سوال عنوان سوال عنوان سوال </h3>
							<p>یکی دو خط توضیح  یکی دو خط توضیح   یکی دو خط توضیح   یکی دو خط توضیح یکی دو خط توضیح </p>
							<span class="rating">
								<i class="icon-eye" style="color: #000"></i>
								<small>(145)</small>
							</span>
							<span class="rating">
								<i class="icon-calendar" style="color: #000"></i>
								<small>10 / 10 / 96</small>
							</span>
							<ul>
								<li></li>
								<li><a href="{{ url('/doctor/FAQ/slug') }}">خواندن بیشتر </a></li>
							</ul>
						</div>

						<div class="strip_list wow fadeIn">
							<a href="#0" class="wish_bt" data-toggle="tooltip" title="افزودن به علاقه ها"></a>
							<figure>
								<a href=""><img src="{{ asset('assets/img/blog-5.jpg')}}" alt=""></a>
							</figure>
							<h3 class="margin-tb-5">عنوان سوال عنوان سوال عنوان سوال عنوان سوال عنوان سوال عنوان سوال </h3>
							<p>یکی دو خط توضیح  یکی دو خط توضیح   یکی دو خط توضیح   یکی دو خط توضیح یکی دو خط توضیح </p>
							<span class="rating">
								<i class="icon-eye" style="color: #000"></i>
								<small>(145)</small>
							</span>
							<span class="rating">
								<i class="icon-calendar" style="color: #000"></i>
								<small>10 / 10 / 96</small>
							</span>
							<ul>
								<li></li>
								<li><a href="{{ url('/doctor/FAQ/slug') }}">خواندن بیشتر </a></li>
							</ul>
						</div>

						<div class="strip_list wow fadeIn">
							<a href="#0" class="wish_bt" data-toggle="tooltip" title="افزودن به علاقه ها"></a>
							<figure>
								<a href=""><img src="{{ asset('assets/img/blog-5.jpg')}}" alt=""></a>
							</figure>
							<h3 class="margin-tb-5">عنوان سوال عنوان سوال عنوان سوال عنوان سوال عنوان سوال عنوان سوال </h3>
							<p>یکی دو خط توضیح  یکی دو خط توضیح   یکی دو خط توضیح   یکی دو خط توضیح یکی دو خط توضیح </p>
							<span class="rating">
								<i class="icon-eye" style="color: #000"></i>
								<small>(145)</small>
							</span>
							<span class="rating">
								<i class="icon-calendar" style="color: #000"></i>
								<small>10 / 10 / 96</small>
							</span>
							<ul>
								<li></li>
								<li><a href="{{ url('/doctor/FAQ/slug') }}">خواندن بیشتر </a></li>
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>




@endsection


@section('d_js')

<script type="text/javascript">
	$('.tabs_styled_2 ul li:nth-child(3) a').addClass('active');
</script>

@endsection