

@extends('layouts.doctor_dashboard')

@section('d_css')

<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendor/jquery.dataTables.min.css') }}">

@endsection

@section('d_content')




<!-- /tab_2 -->

<div class="tab-pane fade show active" >



	<div class="tabs_styled_3">
		<ul class="nav nav-tabs" role="tablist">
			
			<li class="">
				<a href="{{ url('/specialist/ticket') }}" class="nav-link " >اطلاعات</a>
			</li>
			<li class="">
				<a href="{{ url('/specialist/ticket/reserved') }}" class="nav-link active" >نوبت های رزرو شده ی امروز</a>
			</li>
			<li class="">
				<a href="{{ url('/specialist/ticket/setting') }}" class="nav-link" >تنظیم نوبت ها  </a>
			</li>
			
		</ul>
		<!--/nav-tabs -->

		<div class="tab-content">



			<div class="main_title_4">
				<h3><i class="icon_circle-slelected"></i>  13 نوبیت رزرو شده  </h3>
			</div>
			<div class="reviews-container">
				<div class="row">
					<div class="col-lg-12">
						<table id="dynamic-table" class="table striped bordered responsive-table margin-bottom-20">
							<thead>
								<tr>
									<th>مشخصات</th>
									<th>ساعت رزور</th>
									<th>روز رزور</th>
									<th>وضعیت</th>
									<th>عملیات</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><span>اسم و فامیلش</span></td>
									<td><span>9:30 صبح</span></td>
									<td><span>یکشنبه 11 اردیبهشت</span></td>
									<td>
										<span class="label label-success">فعال</span>
									</td>
									<td>
										<a href="" class="btn btn-xs danger-light" data-toggle="tooltip" title="کنسل کردن این نوبت"><i class="pe-7s-close-circle" style="font-size: 21px"></i></a>
										<a href="" class="btn btn-xs danger-light" data-toggle="tooltip" title="افزودن این کاربر به لیست سیاه"><i class="pe-7s-delete-user" style="font-size: 21px"></i></a>
										{{-- <a href="" class="btn btn-xs danger-light"><i class=""></i></a>
										<a href="" class="btn btn-xs danger-light"><i class=""></i></a> --}}
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<!-- /row -->
			</div>
			<!-- End review-container -->
		</div>
	</div>
</div>




@endsection


@section('d_js')
<script type="text/javascript" src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
        jQuery(function ($) {
            //initiate dataTables plugin
            var myTable =
            $('#dynamic-table')
            //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
            .DataTable({
                bAutoWidth: false,
                "aoColumns": [
                  { "bSortable": false },
                  null, null, null, 
                  { "bSortable": false }
                ],
                "aaSorting": [],


                //"bProcessing": true,
                //"bServerSide": true,
                //"sAjaxSource": "http://127.0.0.1/table.php"	,

                //,
                //"sScrollY": "200px",
                //"bPaginate": false,

                //"sScrollX": "100%",
                //"sScrollXInner": "120%",
                //"bScrollCollapse": true,
                //Note: if you are applying horizontal scrolling (sScrollX) on a ".table-bordered"
                //you may want to wrap the table inside a "div.dataTables_borderWrap" element

                //"iDisplayLength": 50


                select: {
                    style: 'multi'
                }
            });

        })
    </script>


<script type="text/javascript">
	$('.tabs_styled_2 ul li.nav-item:nth-child(4) a').addClass('active');
</script>

@endsection