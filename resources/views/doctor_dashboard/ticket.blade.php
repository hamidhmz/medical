

@extends('layouts.doctor_dashboard')

@section('d_css')


@endsection

@section('d_content')




<!-- /tab_2 -->

<div class="tab-pane fade show active" >



	<div class="tabs_styled_3">
		<ul class="nav nav-tabs" role="tablist">
			
			<li class="">
				<a href="{{ url('/specialist/ticket') }}" class="nav-link active" >اطلاعات</a>
			</li>
			<li class="">
				<a href="{{ url('/specialist/ticket/reserved') }}" class="nav-link " >نوبت های رزرو شده ی امروز</a>
			</li>
			<li class="">
				<a href="{{ url('/specialist/ticket/setting') }}" class="nav-link" >تنظیم نوبت ها  </a>
			</li>
			
		</ul>
		<!--/nav-tabs -->

		<div class="tab-content">



			
			<!-- End review-container -->
		</div>
	</div>
</div>




@endsection


@section('d_js')



<script type="text/javascript">
	$('.tabs_styled_2 ul li.nav-item:nth-child(4) a').addClass('active');
</script>

@endsection