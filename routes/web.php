<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::get('/' , 'LinkController@index');//main


Route::get('/search/list' , 'LinkController@index2');//two version of search sight
Route::get('/search/grid' , 'LinkController@index3');


Route::get('/articles' , 'LinkController@index31');





Route::get('/doctor/detail' , 'LinkController@index4');// every body can see doktor prefix(like a profile)
Route::get('/doctor/articles' , 'LinkController@index5');
Route::get('/doctor/articles/slug' , 'LinkController@index5_1');
Route::get('/doctor/FAQ' , 'LinkController@index6');
Route::get('/doctor/FAQ/slug' , 'LinkController@index6_1');
Route::get('/doctor/ticket' , 'LinkController@index7');
Route::get('/doctor/question' , 'LinkController@index8');


Route::get('/specialist/dashboard' , 'LinkController@index9');//just doktor can see own specialist(like a pannel)
Route::get('/specialist/dashboard/setting' , 'LinkController@index10');
Route::get('/specialist/articles' , 'LinkController@index11');
Route::get('/specialist/articles/new' , 'LinkController@index12');
Route::get('/specialist/FAQ' , 'LinkController@index13');
Route::get('/specialist/FAQ/new' , 'LinkController@index14');
Route::get('/specialist/ticket' , 'LinkController@index15');
Route::get('/specialist/ticket/reserved' , 'LinkController@index16');
Route::get('/specialist/questions' , 'LinkController@index17');
Route::get('/specialist/questions/answered' , 'LinkController@index18');



Route::get('/user/dashboard' , 'LinkController@index19');
Route::get('/user/records' , 'LinkController@index20');
Route::get('/user/favorites' , 'LinkController@index21');
Route::get('/user/ticket' , 'LinkController@index22');
Route::get('/user/Leaders' , 'LinkController@index23');
Route::get('/user/MyLeader' , 'LinkController@index24');
Route::get('/user/questions' , 'LinkController@index25');
Route::get('/user/questions/answered' , 'LinkController@index26');
Route::get('/user/dashboard/setting' , 'LinkController@index27');



Route::get('/Leader/dashboard' , 'LinkController@index28');//leader dashboard
Route::get('/Leader/dashboard/setting' , 'LinkController@index29');
Route::get('/Leader/patients' , 'LinkController@index30');

Auth::routes();


Route::get('/changelang/{lang}','SiteController@locarization');//for change language